//
//  LoginViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/3/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IntroViewController.h"
#import "CBG.h"
#import "MotherViewController.h"
#import "Canvas.h"

@interface LoginViewController : MotherViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *tf_email;
@property (strong, nonatomic) IBOutlet UITextField *tf_pass;
@property (strong, nonatomic) IBOutlet CSAnimationView *btnView;
@property (strong, nonatomic) IBOutlet CSAnimationView *loginView;

- (IBAction)onSigninBtn:(id)sender;
- (IBAction)onSocialBtn:(UIButton *)sender;
- (IBAction) unwindSignUp: (UIStoryboardSegue *) segue;
- (IBAction)onRegBtn:(id)sender;
- (IBAction)onAnimBtn:(id)sender;



@end
