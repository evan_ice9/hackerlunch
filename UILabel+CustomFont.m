//
//  UILabel+UILabel_CustomFont.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/2/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "UILabel+CustomFont.h"

@implementation UILabel (CustomFont)

- (NSString *) fontName {
    return self.font.fontName;
}

- (void) setFontName:(NSString *)fontName {
    self.font = [UIFont fontWithName:fontName size:self.font.pointSize];
}

@end
