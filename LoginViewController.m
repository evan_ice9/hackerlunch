//
//  LoginViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/3/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "LoginViewController.h"

#define BTN_FB_TAG 10
#define BTN_TW_TAG 11

@interface LoginViewController ()

@end

@implementation LoginViewController {
    BOOL noIntro;
    Network *net;
}

@synthesize tf_email, tf_pass;

- (id)initWithNibName:(NSString *) nibNameOrNil bundle:(NSBundle *) nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    //[[SIAlertView appearance] setMessageFont:[UIFont fontWithName:@"ProximaNova-Regular" size:14]];
    [[SIAlertView appearance] setTransitionStyle:SIAlertViewTransitionStyleFade];
    net =[[Network Shared_Client] initWithBaseURL:[NSURL URLWithString:HACKERLUNCH_SERVER]];
    

    tf_email.delegate = self;
    tf_pass.delegate = self;

//    FBSession *fbSession = [PFFacebookUtils session];
//    [fbSession closeAndClearTokenInformation];
//    [PFUser logOut];
    
    //[self retrieveLocationAndUpdateBackgroundPhoto];
    
    noIntro = NO;

}

-(void)viewDidAppear:(BOOL) animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toIntroView:) name:SIGNEDUP_NEXTVIEW object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toHomeView:) name:INTRO_NEXTVIEW object:nil];
}

- (void)toHomeView:(NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self performSegueWithIdentifier:@"toHome" sender:nil];
}

- (void)toIntroView:(NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    NSDictionary *userinfo = notification.userInfo;
    NSString *val = userinfo[@"socialValue"];

    [self performSegueWithIdentifier:@"toIntro" sender:val];
}

- (BOOL)textFieldShouldReturn:(UITextField *) textField {

    if(textField == tf_email){
        [tf_pass becomeFirstResponder];
    }else if(textField == tf_pass){
        [self.view endEditing:YES];
        [self checkLoginData];
    }

    return YES;
}

- (void)checkLoginData {
    NSString *email = [tf_email text];
    NSString *pass = [tf_pass text];

    if(![email matches:kEmailRegex]){
        [self showAlert:@"Invalid email address."];
    }else if(pass.length == 0){
        [self showAlert:@"Invalid password."];
    }else {
        [self beingLoginProcessWithEmail:email password:pass];
    }
}

- (void)beingLoginProcessWithEmail:(NSString *) email password:(NSString *) pass {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.labelText = @"Signing in...";
    [hud show:YES];
    
    NSLog(@"%@",email);

    [self.view addSubview:hud];
    
    NSDictionary *data=[[NSDictionary alloc]initWithObjectsAndKeys:email,@"email",pass,@"password",nil];
    NSLog(@"%@",data);
    
   [net loginwithParameters:data completionBlock:^(int status, id response) {
       
       NSLog(@"%d",status);
       switch (status) {
           case 1:
               
               [self showAlert:@"Invalid Email or Password"];
               
               break;
               
           case 2:
               
               [self showAlert:@"The network connection was lost."];
               
               break;
               
           case 0:
               
               [self.tf_pass setText:@""];
               [self performSegueWithIdentifier:@"toHome" sender:nil];
               
               
               NSLog(@"the user %@",response);
               [[UserProfile sharedInstance] setUserObject:email forKey:kUserEmail];
               if (response)
               {
                   [[UserProfile sharedInstance]setUserProfile:response];
               }
               
               break;
           default:
               break;
       }
       
               
               
               
               [hud hide:YES];
               [hud removeFromSuperview];
       
       NSLog(@"parsed userData : %@ " ,[[UserProfile sharedInstance] parseUserData]);
   }];

    
    
    
   /* [PFUser logInWithUsernameInBackground:email password:pass block:^(PFUser *user, NSError *error) {
        if(error){
            if(error.code == 100)
                [self showAlert:@"The network connection was lost."];
            else {
                NSString *errorString = [error userInfo][@"error"];
                [self showAlert:errorString];
                NSLog(@"----> %@", errorString);
            }
        }else{
            [self.tf_pass setText:@""];
            [self performSegueWithIdentifier:@"toHome" sender:nil];
            //[self performSegueWithIdentifier:@"toIntro" sender:nil];

//            [NSUserDefaults setUserValue:email forKey:kUserEmail];
            //[NSUserDefaults setUserValue:user.objectId forKey:kUserObjectId];
            
            [[UserProfile sharedInstance] setUserObject:email forKey:kUserEmail];
        }
    
    

        [hud hide:YES];
        [hud removeFromSuperview];
    }];*/
}

-(void) showAlert:(NSString *) message{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Error" andMessage:message];
    [alertView addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeDestructive handler:nil];
    [alertView show];
}


- (IBAction)onSigninBtn:(id)sender {
    [self checkLoginData];
}

- (void)prepareForSegue:(UIStoryboardSegue *) segue sender:(id) sender {
    [super prepareForSegue:segue sender:sender];
    NSString *val = (NSString *) sender;

    if([segue.destinationViewController isKindOfClass:IntroViewController.class]) {
        IntroViewController *ivc = (IntroViewController *) segue.destinationViewController;

        ivc.isViaFacebook = [val isEqualToString:@"fb"];
        ivc.isViaTwitter = [val isEqualToString:@"tw"];
    }

    [[UserProfile sharedInstance] setUserObject:@YES forKey:kLoggedIn];
    [[UserProfile sharedInstance] setUserObject:@([val isEqualToString:@"fb"]) forKey:kIsViaFacebook];
}

- (IBAction)onSocialBtn:(UIButton *) sender {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];

    if(sender.tag == BTN_FB_TAG){
        hud.labelText = @"Connecting to Facebook...";
        [hud show:YES];

        NSArray *permissionsArray = @[@"email",@"public_profile"];
        
        
        if (FBSession.activeSession.state == FBSessionStateOpen
            || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
            
            // Close the session and remove the access token from the cache
            // The session state handler (in the app delegate) will be called automatically
            [FBSession.activeSession closeAndClearTokenInformation];
            
            // If the session state is not any of the two "open" states when the button is clicked
        } else {
            // Open a session showing the user the login UI
            // You must ALWAYS ask for public_profile permissions when opening a session
            [FBSession openActiveSessionWithReadPermissions:permissionsArray
                                               allowLoginUI:YES
                                          completionHandler:
             ^(FBSession *session, FBSessionState state, NSError *error) {
                 
                 // Retrieve the app delegate
                 AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
                 // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
                 [appDelegate sessionStateChanged:session state:state error:error];
             }];
        }
        
        
        //NSArray *permissionsArray = @[@"public_profile", @"email", @"user_friends"];

        // Login PFUser using facebook

        /*[PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
            
            [hud hide:YES];
            [hud removeFromSuperview];

            if (!user) {
                if (!error) {
                    NSLog(@"Uh oh. The user cancelled the Facebook login.");
                    [self showAlert:@"User cancelled the Facebook login"];
                } else {
                    NSLog(@"Uh oh. An error occurred: %@", error);
                    [self showAlert:@"Something's not quite right..."];
                }
            } else {
                NSLog(@"User with facebook logged in!");

                //[NSUserDefaults setUserValue:user.objectId forKey:kUserObjectId];
                noIntro = !user.isNew;

                [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *err) {
                    [hud show:NO];
                    [hud removeFromSuperview];

                    if (!error) {
//                        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Congratulation!" andMessage:@"Successfully Logged In"];
//                        [alertView addButtonWithTitle:@"Continue" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView * alertView1){
//                            [self.tf_pass setText:@""];
//
//                            if(noIntro)
//                                [self performSegueWithIdentifier:@"toHome" sender:@"fb"];
//                            else [self performSegueWithIdentifier:@"toIntro" sender:@"fb"];
//                        }];
//                        [alertView show];

                        if(noIntro)
                            [self performSegueWithIdentifier:@"toHome" sender:@"fb"];
                        else [self performSegueWithIdentifier:@"toIntro" sender:@"fb"];


                        [[UserProfile sharedInstance] setUserObject:result[@"name"] forKey:kUserFullName];
                        [[UserProfile sharedInstance] setUserObject:result[@"email"] forKey:kUserEmailForFb];
                        [[UserProfile sharedInstance] setUserObject:result[@"id"] forKey:kFacebookId];

                        user[@"fbEmail"] = result[@"email"];
                        user[@"facebookId"] = result[@"id"];
                        [user saveInBackground];

                    }else{
                        NSLog(@"Uh oh. An error occurred: %@", error);
                        [self showAlert:@"Something's not quite right..."];
                    }
                }];
            }
        }];*/
    } else if(sender.tag == BTN_TW_TAG){

    }
}


- (IBAction)unwindSignUp:(UIStoryboardSegue *) segue {
}

- (IBAction)onRegBtn:(id)sender {
    [self performSegueWithIdentifier:@"toSignup" sender:nil];
}

- (IBAction)onAnimBtn:(id)sender {
    if(self.btnView.alpha == 1.0){
        self.btnView.type = CSAnimationTypeFadeOut;
        self.loginView.type = CSAnimationTypeFadeIn;
        [self.btnView startCanvasAnimation];
        [self.loginView startCanvasAnimation];
    }else {
        self.btnView.type = CSAnimationTypeFadeIn;
        self.loginView.type = CSAnimationTypeFadeOut;
        [self.btnView startCanvasAnimation];
        [self.loginView startCanvasAnimation];
    }
}

- (void)touchesEnded:(NSSet *) touches withEvent:(UIEvent *) event {
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
