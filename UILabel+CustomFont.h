//
//  UILabel+UILabel_CustomFont.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/2/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (CustomFont)

@property (nonatomic, copy) NSString* fontName;

@end
