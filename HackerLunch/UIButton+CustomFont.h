//
//  UIButton+CustomFont.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/4/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (CustomFont)

@property (nonatomic, copy) NSString* fontName;
@property (nonatomic, copy) NSString* cornerRadiusSize;

@end
