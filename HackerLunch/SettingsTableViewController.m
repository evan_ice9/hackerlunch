//
//  SettingsTableViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/17/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "HomeViewController.h"

@interface SettingsTableViewController ()

@end

@implementation SettingsTableViewController {
    HomeViewController *homeViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL) animated {

    self.fullNameView.text = [[UserProfile sharedInstance] getFullName];
}

- (void)viewWillAppear:(BOOL) animated {
    UIImage *img = [[UserProfile sharedInstance] getProfilePicture];

    if(img != nil){
        self.profilePicView.image = img;
    }
}

-(void) setHomeView:(UIViewController *) vc{
    homeViewController = (HomeViewController *) vc;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return 7;
//}


//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
//
//    UIView *bgColorView = [[UIView alloc] init];
//    bgColorView.backgroundColor = [UIColor blackColor];
//    [cell setSelectedBackgroundView:bgColorView];
//
//    if(indexPath.row < 2){
//        bgColorView.backgroundColor = [UIColor colorWithHexString:@"333333"];
//        [cell setSelectedBackgroundView:bgColorView];
//    }
//
//    return cell;
//}


- (void)tableView:(UITableView *) tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *) indexPath {
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithHexString:@"222222"];
    [cell setSelectedBackgroundView:bgColorView];
}


- (void)tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *) indexPath {
    //[super tableView:tableView didSelectRowAtIndexPath:indexPath];
    HomeViewController *newTopViewController;

    NSInteger index = indexPath.row;

    NSLog(@"index: %i",index);

    switch(index){
        case 2: //home
            if (![self.slidingViewController.topViewController isKindOfClass:[HomeViewController class]]) {
                newTopViewController = homeViewController;
                [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
                    CGRect frame = self.slidingViewController.topViewController.view.frame;
                    self.slidingViewController.topViewController = newTopViewController;
                    self.slidingViewController.topViewController.view.frame = frame;
                    [self.slidingViewController resetTopView];
                }];
            }else{
                [self.slidingViewController resetTopView];
            }
            break;
        case 3: //History
            if (![self.slidingViewController.topViewController isKindOfClass:[HistoryViewController class]]) {
                newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"historyView"];
                [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
                    CGRect frame = self.slidingViewController.topViewController.view.frame;
                    self.slidingViewController.topViewController = newTopViewController;
                    self.slidingViewController.topViewController.view.frame = frame;
                    [self.slidingViewController resetTopView];
                }];
            }else{
                [self.slidingViewController resetTopView];
            }
            break;
        case 4: //Messages
            [self.slidingViewController anchorTopViewTo:ECLeft];
            break;
        case 5: //My profile
            if (![self.slidingViewController.topViewController isKindOfClass:[MyProfileViewController class]]) {
                newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"myProfileView"];
                [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
                    CGRect frame = self.slidingViewController.topViewController.view.frame;
                    self.slidingViewController.topViewController = newTopViewController;
                    self.slidingViewController.topViewController.view.frame = frame;
                    [self.slidingViewController resetTopView];
                }];
            }else{
                [self.slidingViewController resetTopView];
            }
            break;
        case 6: //Settings
            if (![self.slidingViewController.topViewController isKindOfClass:[PrefsViewController class]]) {
                newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"prefsView"];
                [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
                    CGRect frame = self.slidingViewController.topViewController.view.frame;
                    self.slidingViewController.topViewController = newTopViewController;
                    self.slidingViewController.topViewController.view.frame = frame;
                    [self.slidingViewController resetTopView];
                }];
            }else{
                [self.slidingViewController resetTopView];
            }
            break;
        case 7: //Logout
            [[NSNotificationCenter defaultCenter] postNotificationName:kLogOutUser object:nil];
            break;

        default:break;
    }
}


@end
