//
//  MyProfileViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 9/15/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Canvas.h"
#import "AsyncImageView.h"
#import "MotherViewController.h"

@interface MyProfileViewController : MotherViewController <UITextViewDelegate, MBProgressHUDDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (strong, nonatomic) IBOutlet CSAnimationView *descView;
@property (strong, nonatomic) IBOutlet UIView *btnDesc;
@property (strong, nonatomic) IBOutlet UIView *btnFacebook;
@property (strong, nonatomic) IBOutlet AsyncImageView *profilePicView;
@property (strong, nonatomic) IBOutlet UITextView *input_description;
@property (strong, nonatomic) IBOutlet UITextField *input_description_bg;
@property (strong, nonatomic) IBOutlet UILabel *sublabel;
@property (strong, nonatomic) IBOutlet UIButton *btnConnect;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;

- (IBAction)onButtPress:(UIButton *)sender;
- (IBAction)onFbConnect:(UIButton *)sender;

@end
