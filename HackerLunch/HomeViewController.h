//
//  HomeViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/11/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "MotherViewController.h"
#import "ECSlidingViewController.h"
#import "ChatHistoryViewController.h"

@interface HomeViewController : MotherViewController <MBProgressHUDDelegate>

@property (strong, nonatomic) IBOutlet AsyncImageView *propicVIew;
@property (strong, nonatomic) IBOutlet UIView *dot;


- (IBAction)onNavButton:(UIButton *)sender;
- (IBAction)onPairMe:(UIButton *)sender;
@end
