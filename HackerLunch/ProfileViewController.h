//
//  ProfileViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/21/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "PairViewController.h"

@interface ProfileViewController : MotherViewController

@property (strong, nonatomic) PFObject *userObject;
@property (strong, nonatomic) PFUser *user;
@property(nonatomic, strong) UIImage *userImage;

@property (strong, nonatomic) IBOutlet AsyncImageView *profilePic;
@property (strong, nonatomic) IBOutlet UIView *socialContainer;
@property (weak, nonatomic) IBOutlet UILabel *userNameView;
@property (weak, nonatomic) IBOutlet UILabel *statView;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *fbLabel;
@property (strong, nonatomic) IBOutlet UIImageView *fbIcon;

- (IBAction)onHaveLunch:(id)sender;
- (IBAction)onNavBtn:(UIButton *)sender;

@end
