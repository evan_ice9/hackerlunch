//
//  Network.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 1/17/15.
//  Copyright (c) 2015 Fahim Ahmed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFHTTPSessionManager.h>

@interface Network : AFHTTPSessionManager

+(Network *)Shared_Client;
-(instancetype)initWithBaseURL:(NSURL *)url;
-(void)loginwithParameters:(NSDictionary *)data completionBlock:(void (^)(int status,id response))compblock;


@end
