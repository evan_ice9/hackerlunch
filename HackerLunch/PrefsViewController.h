//
//  PrefsViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 9/17/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MotherViewController.h"
#import "Canvas.h"
#import "ActionSheetStringPicker.h"

@class CSAnimationView;

@interface PrefsViewController : MotherViewController <UITextFieldDelegate, UIActionSheetDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet UIView *viewDistance;
@property (strong, nonatomic) IBOutlet UIView *viewEmail;
@property (strong, nonatomic) IBOutlet UIView *viewPassword;
@property (strong, nonatomic) IBOutlet CSAnimationView *viewInput;
@property (strong, nonatomic) IBOutlet UITextField *tfInput;
@property (weak, nonatomic) IBOutlet UILabel *distanceTitle;


- (IBAction)onButton:(UIButton *)sender;
- (IBAction)onNotificationSwitch:(UISwitch *)sender;
- (IBAction)onInputButton:(UIButton *)sender;

@end
