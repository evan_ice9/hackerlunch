//
//  MotherViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/5/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BackgroundPhotoSingleton.h"

@interface MotherViewController : UIViewController

@property(nonatomic, strong) UIView *overlayView_blue;
@property(nonatomic, strong) UIView *overlayView_orange;
@property(nonatomic, strong) UIImageView *bgImageView;


- (void)switchColorToOrange;

- (void)switchColor;
@end
