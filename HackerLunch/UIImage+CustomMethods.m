//
//  UIImage+CustomMethods.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/7/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "UIImage+CustomMethods.h"

@implementation UIImage (CustomMethods)

- (UIImage *)crop:(CGRect)rect {
    
    rect = CGRectMake(rect.origin.x*self.scale,
                      rect.origin.y*self.scale,
                      rect.size.width*self.scale,
                      rect.size.height*self.scale);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef
                                          scale:self.scale
                                    orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}


@end
