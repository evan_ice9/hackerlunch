//
//  ChatHistoryViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 9/8/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AsyncImageView/AsyncImageView.h>
#import <APUtils/UIColor+APUtils.h>

@interface ChatHistoryViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, MBProgressHUDDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *errButt;
@property (strong, nonatomic) IBOutlet UILabel *pairInfoLabel;

- (IBAction)onErrButt:(UIButton *)sender;

@end
