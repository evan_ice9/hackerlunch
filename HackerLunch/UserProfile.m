//
//  UserProfile.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/26/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

//#import "UserProfile.h"

#import <Foundation/Foundation.h>

@interface UserProfile ()

@end

@implementation UserProfile {
    //BOOL runOnce;
}

@synthesize runOnce;

//-(id) init {
//    self = [self init];
//    runOnce = NO;
//
//    return self;
//}

+ (UserProfile *)sharedInstance {
    static UserProfile *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        NSLog(@"USER PROFILER INITED");
        sharedInstance = [[UserProfile alloc] init];
    });
    
    return sharedInstance;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(NSDictionary *) parseUserData{
    
    NSDictionary *userData=[[NSUserDefaults standardUserDefaults]objectForKey:kUserProfile];
    
    //PFUser *user = [PFUser currentUser];
    //PFFile *file = user[kUserProfilePicture];

    /*NSString *uid = user.username;
    NSString *name = user[@"fullName"];
    NSString *image = file.url;*/
    
    
    NSString *uid = [userData objectForKey:@"id"];
    NSString *name = [NSString stringWithFormat:@"%@ %@",userData[@"firstname"],userData[@"lastname"]];// userData[@"fullName"];
    NSString *image = userData[@"photo_url"];
    
    if(image == nil)
        image = @"";

    //if (image == nil) image = [UIImage imageNamed:@"profile_mini_image_placeholder"];

    return @{@"uid":uid, @"image":image, @"name":name};
}



-(void)setUserProfile:(NSDictionary *)data
{
    [[NSUserDefaults standardUserDefaults]setObject:data forKey:kUserProfile];

}

-(void) checkAndSetUserName{
    //runOnce = YES;
    BOOL isViaFb = [[[UserProfile sharedInstance] userObjectForKey:kIsViaFacebook] isEqualToString:@"email"]?0:1;
    
    /*PFUser *user = [PFUser currentUser];

    if(isViaFb){
        if(![user.username matches:kEmailRegex]){
            user.username = [[UserProfile sharedInstance] userObjectForKey:kUserEmailForFb];  //[NSUserDefaults valueForKey:kUserEmailForFb];
            
            [user saveEventually:^(BOOL succeeded, NSError *error) {
                if(succeeded){
                    NSLog(@"SUCCESSFULLY USERNAME CHANGED");

                    [self registerAtFireBaseWithEmail:user.username password:user.objectId];
                }
            }];
        }else{
            [self registerAtFireBaseWithEmail:user.username password:user.objectId];
        }
    }else{
        [self registerAtFireBaseWithEmail:user.username password:user.objectId];
    }

    [self registerForPush];*/
}

- (void)registerForPush {
    PFInstallation *installation = [PFInstallation currentInstallation];

    [installation setObject:[PFUser currentUser] forKey:@"User"];
    [installation saveInBackground];
}

- (void)unregisterForPush {
    PFInstallation *installation = [PFInstallation currentInstallation];
    
    [installation setObject:[NSNull null] forKey:@"User"];
    [installation saveInBackground];
}

-(void) registerAtFireBaseWithEmail:(NSString *) email password:(NSString *)password{
    //runOnce = YES;

    Firebase* myRef = [[Firebase alloc] initWithUrl:FIREBASE];
    FirebaseSimpleLogin* authClient = [[FirebaseSimpleLogin alloc] initWithRef:myRef];

   // [authClient logout];

    //check user status
    [authClient checkAuthStatusWithBlock:^(NSError* error, FAUser* user) {

        if (error != nil) {
            // an error occurred while attempting login
            NSLog(@"%@", error);
            //[self showAlert:@"Somethings not right..."];
            [authClient loginWithEmail:email andPassword:password withCompletionBlock:^(NSError *err, FAUser *usr) {
                if(err){
                    NSLog(@"error: %@", err);
                }else{
                    NSLog(@"Successfully Logged in: %@", user);
                    [[NSNotificationCenter defaultCenter] postNotificationName:FIREBASE_AUTH object:nil userInfo:nil];
                }
            }];
            
        } else if (user == nil) {
            [authClient loginWithEmail:email andPassword:password withCompletionBlock:^(NSError *err, FAUser *usr) {
                if(err.code == FAErrorUserDoesNotExist){
                    [authClient createUserWithEmail:email password:password andCompletionBlock:^(NSError *errr, FAUser *_user) {
                        if(errr){
                            NSLog(@"error: %@", errr);
                            //[self showAlert:@"Somethings not right..."];

                        }else{
                            NSLog(@"newUser at firebase");

//                            NSDictionary *newUser = @{
//                                    @"user_id": _user.userId,
//                                    @"provider": [[NSNumber alloc] initWithInt:_user.provider],
//                                    @"email": _user.email
//                            };
//
//                            NSString *str = [NSString stringWithFormat:@"users/%@", _user.userId];
//
//                            [[myRef childByAppendingPath:str] setValue:newUser];

                            [self registerAtFireBaseWithEmail:email password:password];
                        }
                    }];
                }else if(err){
                    NSLog(@"error: %@", err);
                    //[self showAlert:@"Somethings not right..."];
                }else{
                    NSLog(@"Successfully Logged in: %@", user);
                    [[NSNotificationCenter defaultCenter] postNotificationName:FIREBASE_AUTH object:nil userInfo:nil];
                }
            }];
        } else {
            // user authenticated with Firebase
            NSLog(@"%@", user);
            [[NSNotificationCenter defaultCenter] postNotificationName:FIREBASE_AUTH object:nil userInfo:nil];
        }
    }];
}

-(void)initChatRoom:(PFUser *) pairedUser {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.labelText = @"Establishing connection...";
    [self.view addSubview:hud];
    [hud show:YES];

    PFObject *chatLog = [PFObject objectWithClassName:class_chatlog];

    PFUser *currentUser = [PFUser currentUser];
    PFUser *pUser = pairedUser;
    NSString *channelName = [NSString stringWithFormat:@"%@%@", currentUser.objectId, pUser.objectId];

    chatLog[@"userA"] = currentUser;
    chatLog[@"userB"] = pUser;
    chatLog[@"channelName"] = channelName;
    chatLog[@"direction"] = currentUser.objectId;

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [formatter stringFromDate:[NSDate date]];

    chatLog[@"date"] = dateStr;

    NSString *str = [NSString stringWithFormat:@"%@%@",currentUser.objectId, pUser.objectId];
    NSString *rStr = [NSString stringWithFormat:@"%@%@", pUser.objectId, currentUser.objectId];

    PFQuery *query1 = [PFQuery queryWithClassName:class_chatlog];
    [query1 whereKey:@"channelName" containsString:str];

    PFQuery *query2 = [PFQuery queryWithClassName:class_chatlog];
    [query2 whereKey:@"channelName" containsString:rStr];

    PFQuery *query = [PFQuery orQueryWithSubqueries:@[query1, query2]];

    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        BOOL inited;
        if(error){
            if(error.code == kPFErrorObjectNotFound){
                //doesn't exist
                chatLog[@"inited"] = @(NO);

                inited = NO;

                [chatLog saveInBackgroundWithBlock:^(BOOL succeeded, NSError *err) {
                    [hud hide:YES];
                    [hud removeFromSuperview];

                    //chatroom = channelName;

                    //[self loadChatView];
                }];
            }else{
                [hud hide:YES];
                [hud removeFromSuperview];
                NSLog(@"chat room init error.");
                //[self showAlert:error.userInfo[@"error"]];
            }
        }else{
            if(object){
                //already exist
                NSLog(@"%@", object[@"channelName"]);

                //chatLog = object;

                inited = [object[@"inited"] boolValue];

                //chatroom = object[@"channelName"];

                [hud hide:YES];
                [hud removeFromSuperview];


                //[self loadChatView];
            }else{
                NSLog(@"#2 chat room init error.");
            }
        }

    }];
}

-(void) setMaxDistance:(int) val {
    [self setUserObject:@(val) forKey:kMaxDistance];
}

-(int) getMaxDistance {
    int currentValue = [[self userObjectForKey:kMaxDistance] integerValue];
    return !currentValue ? 2 : currentValue;
}

- (UIImage *) getProfilePicture {
    NSData *data = [self userObjectForKey:kLoadProPic];
    UIImage *img = [UIImage imageWithData:data];

    return img;
}

- (NSString *) getFullName {
    //PFUser *user = [PFUser currentUser];
    return [NSString stringWithFormat:@"%@ %@",[self userObjectForKey:kUserFirstName],[self userObjectForKey:kUserLastName]];
}

-(NSString *) getFbPicUrl{
    FBSession *fbSession = [PFFacebookUtils session];
    NSString *accessToken = [fbSession accessTokenData].accessToken;
    NSString *url = [NSString stringWithFormat:@"https://graph.facebook.com/me/picture?type=large&return_ssl_resources=1&access_token=%@", accessToken];

    return url;
}


- (BOOL) setUserObject:(id) object forKey:(NSString *) key {

    if(object == nil)
        return false;

    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictionary = [[userDefault objectForKey:kUserProfile] mutableCopy];

    if(dictionary == nil){
        dictionary = [[NSMutableDictionary alloc] init];
    }

    dictionary[key] = object;
   // [dictionary setObject:object forKey:key];

    [userDefault setObject:dictionary forKey:kUserProfile];

    return [userDefault synchronize];
}

- (void) removeUserObjectForKey:(NSString *) key{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictionary = [[userDefault objectForKey:kMainDictionary] mutableCopy];

    if(dictionary) {
        [dictionary removeObjectForKey:key];
        [userDefault setObject:dictionary forKey:kMainDictionary];
        [userDefault synchronize];
    }
}

- (id) userObjectForKey:(NSString *) key
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictionary = [userDefault objectForKey:kUserProfile];

    return dictionary[key];
}

- (BOOL) resetUserInfo {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:kUserProfile];

    NSLog(@"Destroying current user..."); //adios ****

    return [userDefault synchronize];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setLastMsgId:(NSInteger) val Key:(NSString *) key {
    //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictionary = [[self userObjectForKey:kLastMessageId] mutableCopy];

    if(dictionary == nil)
        dictionary = [[NSMutableDictionary alloc] init];

    dictionary[key] = @(val);

    [self setUserObject:dictionary forKey:kLastMessageId];
}

- (NSInteger) getLastMsgId:(NSString *) key {
   // NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictionary = [self userObjectForKey:kLastMessageId];

    if(dictionary[key] == nil)
        return (NSInteger) -1;
    return [dictionary[key] integerValue];
}


-(void) goOffline {
    if([PFUser currentUser]){
        Firebase *ref = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@/users", FIREBASE]];

        NSDictionary *userInf = @{
            @"uid": [PFUser currentUser].username,
            @"presence": @(NO)
            };

        NSString *tmp = [PFUser currentUser].objectId;
        [[ref childByAppendingPath:[NSString stringWithFormat:@"%@", tmp]] setValue:userInf];
    }
}

- (void)goOnline {
    if([PFUser currentUser]) {
        Firebase *ref = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@/users", FIREBASE]];

        NSDictionary *userInf = @{
                @"uid" : [PFUser currentUser].username,
                @"presence" : @(YES)
        };

        NSString *tmp = [PFUser currentUser].objectId;
        [[ref childByAppendingPath:[NSString stringWithFormat:@"%@", tmp]] setValue:userInf];
    }
}
@end
