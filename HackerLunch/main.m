//
//  main.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 6/30/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
