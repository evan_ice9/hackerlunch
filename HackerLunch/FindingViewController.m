//
//  FindingViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/20/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "FindingViewController.h"
#import "PairViewController.h"

@interface FindingViewController ()

@end

@implementation FindingViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showFailed:) name:kNoUserFound object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentPairView:) name:kPairUserController object:nil];
}

- (void)presentPairView:(NSNotification *) not {
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    PairViewController *newVc = (PairViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"pairView"];

    NSDictionary *dict = not.userInfo;
    newVc.userObject = dict[kUserObject];
    newVc.userImage = dict[kUserImage];
    newVc.user = dict[kPairedUser];

    self.slidingViewController.topViewController.view.frame = frame;

    [UIView transitionWithView:self.slidingViewController.topViewController.view.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        self.slidingViewController.topViewController = newVc;
                    }
                    completion:(void (^)(BOOL)) ^{
                        [self.slidingViewController resetTopView];
                    }];
}


- (void)showFailed:(NSNotification *) not {
    self.noFindView.type = CSAnimationTypeFadeIn;
    self.findingView.type = CSAnimationTypeFadeOut;

    [self.findingView startCanvasAnimation];
    [self.noFindView startCanvasAnimation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onSideBtn:(UIButton *)sender {
    NSInteger tag = sender.tag;

    switch(tag){
        case 10:
            [self.slidingViewController anchorTopViewTo:ECRight];
            break;
        case 11:
            [self.slidingViewController anchorTopViewTo:ECLeft];
            break;
        default:
            break;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kNoUserFound object:nil userInfo:nil];
}

- (IBAction)onFbInvite:(id)sender {
    [FBRequestConnection startWithGraphPath:@"/me/invitable_friends"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                                  FBRequestConnection *connection,
                                  id result,
                                  NSError *error
                          ) {
                              NSLog(@"error: %@", error.userInfo);
                          }];
}
@end
