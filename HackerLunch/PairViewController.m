//
//  PairViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/17/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "PairViewController.h"

@interface PairViewController ()

@end

@implementation PairViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.picView.image = self.userImage;
    self.userName.text = self.userObject[@"fullName"];

    self.statView.text = self.userObject[@"about"];  //[[UserProfile sharedInstance] userObjectForKey:kMatchedUserData]; //[NSUserDefaults valueForKey:kMatchedUserData];

    //NSLog(@"%@",self.userObject);
    self.dot.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatDot:) name:CHAT_NOTIFICATION object:nil];
}

- (void)chatDot:(NSNotification *) not {
    bool isPending = (bool) not.object;
    self.dot.hidden = !isPending;
}

-(void)viewWillAppear:(BOOL) animated {
    [self switchColorToOrange];

    [[UserProfile sharedInstance] initChatRoom:self.user];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBack:(UIButton *)sender {

    int tag = sender.tag;

    if(tag == 10){
        [self.slidingViewController anchorTopViewTo:ECRight];
    }else if(tag == 11){
        [self.slidingViewController anchorTopViewTo:ECLeft];
    }
    //[self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)onViewProfile:(id)sender {
//    ProfileViewController *newVc = (ProfileViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];;
//    CGRect frame = self.slidingViewController.topViewController.view.frame;
//
//    newVc.user = self.user;
//    newVc.userObject = self.userObject;
//    newVc.userImage = self.userImage;
//
//    self.slidingViewController.topViewController.view.frame = frame;
//
//    [UIView transitionWithView:self.slidingViewController.topViewController.view.window
//                      duration:0.5
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:^{
//                        self.slidingViewController.topViewController = newVc;
//                    }
//                    completion:(void (^)(BOOL)) ^{
//                        [self.slidingViewController resetTopView];
//                    }];
    [[NSNotificationCenter defaultCenter] postNotificationName:LAUNCH_PROFILE_VIEW object:self.user userInfo:nil];
}

- (IBAction)onHaveLunch:(id)sender {
    ChatViewHolderViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"chatHolderView"];

    cvc.roomTitle = self.userObject[@"fullName"];
    cvc.pUser = self.user;
    
    [self presentViewController:cvc animated:YES completion:nil];
}
@end
