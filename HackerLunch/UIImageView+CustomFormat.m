//
//  UIImageView+CustomFormat.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/6/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "UIImageView+CustomFormat.h"

@implementation UIImageView (CustomFormat)

- (NSString *) cornerRadiusSize {
    return self.cornerRadiusSize;
}

- (void) setCornerRadiusSize:(NSString *)cornerRadiusSize {
    self.clipsToBounds = YES;
    self.layer.cornerRadius = [cornerRadiusSize floatValue];
}

@end
