//
//  ChatViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/25/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <APUtils/UIColor+APUtils.h>
#import "ChatViewController.h"
#import "UIView+APUtils.h"

#define MAX_NUM_MSG_LOAD 5
#define MAX_NUM_EARLIER_MSG_LOAD 5

@implementation ChatViewController {
    Firebase *firebase;
    MBProgressHUD *hud;

    NSString *chatroom;
    NSDictionary *userinfo;

    BOOL initialized;
    FirebaseHandle handle;
    FirebaseHandle handle2;

    NSMutableArray *users;
    NSMutableArray *messages;

    UIImageView *outgoingBubbleImageView;
    UIImageView *incomingBubbleImageView;

    NSInteger msgId;
    NSInteger startingPrio;
    NSMutableDictionary *avatars;
}


- (id)initWith:(NSString *)Chatroom Userinfo:(NSDictionary *)Userinfo {
    self = [super init];
    chatroom = Chatroom;
    userinfo = [Userinfo copy];

    self.view.frame = CGRectMake(0, 0, 320, self.view.bounds.size.height - 64);

    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL) animated {
    [super viewDidAppear:animated];

    [self setObservers];

    msgId = [[UserProfile sharedInstance] getLastMsgId:chatroom];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.labelFont = [UIFont fontWithName:@"ProximaNova-Regular" size:14];
    hud.delegate = self;
    hud.removeFromSuperViewOnHide = YES;
    hud.labelText = @"Loading messages...";
    
    [self.view addSubview:hud];
    [hud show:YES];
    
    self.title = chatroom;

    users = [[NSMutableArray alloc] init];
    messages = [[NSMutableArray alloc] init];
    avatars = [[NSMutableDictionary alloc] init];

    self.sender = [userinfo valueForKey:@"uid"];
    
    //NSLog(@"%@", self.pUser);
    
    self.inputToolbar.contentView.leftBarButtonItem = nil;
    self.collectionView.collectionViewLayout.messageBubbleFont = [UIFont fontWithName:@"ProximaNova-Regular" size:13];
    
    outgoingBubbleImageView = [JSQMessagesBubbleImageFactory outgoingMessageBubbleImageViewWithColor:[UIColor colorWithHexString:@"fdf7ee"]];
    incomingBubbleImageView = [JSQMessagesBubbleImageFactory incomingMessageBubbleImageViewWithColor:[UIColor colorWithHexString:@"ecf9fb"]];

    self.collectionView.backgroundColor = [UIColor clearColor];
    self.showLoadEarlierMessagesHeader = YES;

    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleCollectionTapRecognizer:)];
    [self.collectionView addGestureRecognizer:tapRecognizer];

  //  [self loadMessages];

//    self.collectionView.collectionViewLayout.springinessEnabled = NO;

   // self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeMake(35, 35);
   // self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeMake(35, 35);

    firebase = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@/%@", FIREBASE, chatroom]];
    [firebase removeAllObservers];

    [self loadMessages];
//    FQuery* postsQuery = [firebase queryLimitedToNumberOfChildren:MAX_NUM_MSG_LOAD];
//    handle = [postsQuery observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
//        [firebase removeAllObservers];
//        [self loadMessages];
//    }];
}

- (void)loadMessages {

    initialized = NO;
  //  firebase = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@/%@", FIREBASE, chatroom]];

    FQuery* postsQuery = [firebase queryLimitedToNumberOfChildren:MAX_NUM_MSG_LOAD];

    [postsQuery observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
        NSString *text = [snapshot.value objectForKey:@"text"];
        NSString *uid = [snapshot.value objectForKey:@"uid"];
        NSString *dateStr = [snapshot.value objectForKey:@"date"];

        msgId = [[snapshot.value objectForKey:@"priority"] integerValue];

        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
        NSDate *date = [formatter dateFromString:dateStr];

        if(date == nil) date = [NSDate date];

        JSQMessage *message = [[JSQMessage alloc] initWithText:text sender:uid date:date];
        [messages addObject:message];

        NSString *image = [snapshot.value objectForKey:@"image"];
        NSString *name = [snapshot.value objectForKey:@"name"];

        [users addObject:@{@"name" : name, @"image" : image}];
        avatars[uid] = image;

        if(initialized) {
            [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
            [self finishReceivingMessage];
        }
    }];

    handle = [postsQuery observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {

        [firebase removeObserverWithHandle:handle];

        if(snapshot.value != [NSNull null]) {
            self.infoView.hidden = YES;

            startingPrio = snapshot.childrenCount;
            startingPrio = msgId - startingPrio;

            [[UserProfile sharedInstance] setLastMsgId:msgId Key:chatroom];

            [self finishReceivingMessage];
        }else{
            NSLog(@"No messages");
        }

        //[hud show:NO];
        [hud hide:YES];

        initialized = YES;
    }];

    Firebase* connectedRef = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@/.info/connected", FIREBASE]];
    [connectedRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot){
        if([snapshot.value boolValue]) {
            NSLog(@"connected");

            Firebase *ref = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@/users", FIREBASE]];

            NSDictionary *userInf = @{
                    @"uid": userinfo[@"uid"],
                    @"presence": @(YES)
            };

            NSString *tmp = [PFUser currentUser].objectId;
            [[ref childByAppendingPath:[NSString stringWithFormat:@"%@", tmp]] setValue:userInf];

        } else {
            NSLog(@"not connected");
        }
    }];
}

- (void)queryUserPresence:(NSString *) text from:(NSString *) name {
    Firebase *ref = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@/users/%@", FIREBASE, self.pUser.objectId]];
    FirebaseHandle h;

    h = [ref observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        [ref removeObserverWithHandle:h];

        FDataSnapshot *snap;
        NSArray *arr = snapshot.children.allObjects;
        
        if(arr.count > 0){
            snap = arr[0];

            BOOL isOnline = NO;

            if(snap.value != [NSNull null]) {
                int val = [snap.value intValue];

                if(val != 0)
                    isOnline = YES;
            }

            if(!isOnline){
                //user offline
                PFQuery *query = [PFInstallation query];
                [query whereKey:@"User" equalTo:self.pUser];

                NSString *msg = [NSString stringWithFormat:@"%@ from %@", text, name];

                [PFPush sendPushMessageToQueryInBackground:query withMessage:msg];

                NSLog(@"user offline");
            }else{
                NSLog(@"user online: %@", snap.value);
            }
        }
    }];
}

- (void) handleCollectionTapRecognizer:(UITapGestureRecognizer*)recognizer {
    if(recognizer.state == UIGestureRecognizerStateEnded){
        if([self.inputToolbar.contentView.textView isFirstResponder])
            [self.inputToolbar.contentView.textView resignFirstResponder];
    }
}

#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *) button withMessageText:(NSString *) text sender:(NSString *) sender date:(NSDate *) date {
    NSString *uid = [userinfo valueForKey:@"uid"];
    NSString *image = [userinfo valueForKey:@"image"];
    NSString *name = [userinfo valueForKey:@"name"];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    NSString *dateStr = [formatter stringFromDate:date];

    msgId++;

    [[firebase childByAutoId] setValue:@{@"text" : text, @"uid" : uid, @"date" : dateStr, @"image" : image, @"name" : name, @"priority" : @(msgId)} andPriority:@(msgId)];

    [[UserProfile sharedInstance] setLastMsgId:msgId Key:chatroom];
    self.infoView.hidden = YES;

    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    [self finishSendingMessage];

    if(!self.inited){
        self.inited = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"setInited" object:nil userInfo:nil];
    }

    [self queryUserPresence:text from:name];
}



#pragma mark - JSQMessages CollectionView DataSource

- (id <JSQMessageData>)collectionView:(JSQMessagesCollectionView *) collectionView messageDataForItemAtIndexPath:(NSIndexPath *) indexPath {
    return [messages objectAtIndex:indexPath.item];
}

- (UIImageView *)collectionView:(JSQMessagesCollectionView *) collectionView bubbleImageViewForItemAtIndexPath:(NSIndexPath *) indexPath {
    JSQMessage *message = [messages objectAtIndex:indexPath.item];
    if([[message sender] isEqualToString:self.sender]) {
        return [[UIImageView alloc] initWithImage:outgoingBubbleImageView.image highlightedImage:outgoingBubbleImageView.highlightedImage];
    }
    else return [[UIImageView alloc] initWithImage:incomingBubbleImageView.image highlightedImage:incomingBubbleImageView.highlightedImage];
}

- (UIImageView *)collectionView:(JSQMessagesCollectionView *) collectionView avatarImageViewForItemAtIndexPath:(NSIndexPath *) indexPath {
    JSQMessage *msg = [messages objectAtIndex:indexPath.row];

    AsyncImageView *imageView = [[AsyncImageView alloc] initWithImage:[UIImage imageNamed:@"profile_mini_image_placeholder"]];
    //imageView.image = [UIImage imageNamed:@"profile_mini_image_placeholder"];
    imageView.size = CGSizeMake(34, 34);
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    NSString *avatar = avatars[msg.sender];
    if(![avatar isEqualToString:@""])
        imageView.imageURL = [NSURL URLWithString:avatars[msg.sender]];
    
    imageView.layer.cornerRadius = imageView.frame.size.width / 2;
    imageView.layer.masksToBounds = YES;

    return imageView;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *) collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *) indexPath {
    if(indexPath.item % 3 == 0) {
        JSQMessage *message = messages[indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    return nil;
}

//- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *) collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *) indexPath {
//    JSQMessage *message = messages[indexPath.item];
//    if([message.sender isEqualToString:self.sender]) {
//        return nil;
//    }
//
//    if(indexPath.item - 1 > 0) {
//        JSQMessage *previousMessage = messages[indexPath.item - 1];
//        if([[previousMessage sender] isEqualToString:message.sender]) {
//            return nil;
//        }
//    }
//
//    NSDictionary *user = users[indexPath.row];
//    return [[NSAttributedString alloc] initWithString:user[@"name"]];
//}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *) collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *) indexPath {
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *) collectionView numberOfItemsInSection:(NSInteger) section {
    return [messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *) collectionView cellForItemAtIndexPath:(NSIndexPath *) indexPath {
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *) [super collectionView:collectionView cellForItemAtIndexPath:indexPath];

//    JSQMessage *message = [messages objectAtIndex:indexPath.item];
//    if([message.sender isEqualToString:self.sender]) {
//        cell.textView.textColor = [UIColor blackColor];
//    }
//    else {
//        cell.textView.textColor = [UIColor whiteColor];
//    }

    //cell.textView.linkTextAttributes = @{NSForegroundColorAttributeName : cell.textView.textColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid)};

    cell.textView.textColor = [UIColor darkGrayColor];

    return cell;
}

#pragma mark - JSQMessages collection view flow layout delegate

- (CGFloat)collectionView:(JSQMessagesCollectionView *) collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *) collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *) indexPath {
    if(indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *) collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *) collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *) indexPath {
    JSQMessage *message = [messages objectAtIndex:indexPath.item];
    if([[message sender] isEqualToString:self.sender]) {
        return 0.0f;
    }

    if(indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [messages objectAtIndex:indexPath.item - 1];
        if([[previousMessage sender] isEqualToString:[message sender]]) {
            return 0.0f;
        }
    }
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *) collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *) collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *) indexPath {
    return 0.0f;
}

- (void)collectionView:(JSQMessagesCollectionView *) collectionView
                header:(JSQMessagesLoadEarlierHeaderView *) headerView didTapLoadEarlierMessagesButton:(UIButton *) sender {

    NSLog(@"didTapLoadEarlierMessagesButton");

    if(startingPrio > 0) {
        
        [hud show:YES];
        [self.view addSubview:hud];

        __block int i = 0;
        FQuery *postsQ = [[firebase queryStartingAtPriority:@(startingPrio - MAX_NUM_EARLIER_MSG_LOAD)] queryEndingAtPriority:@(startingPrio)];

        handle = [postsQ observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
            [firebase removeObserverWithHandle:handle];

            NSString *text = [snapshot.value objectForKey:@"text"];
            NSString *uid = [snapshot.value objectForKey:@"uid"];
            NSString *dateStr = [snapshot.value objectForKey:@"date"];

            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
            NSDate *date = [formatter dateFromString:dateStr];

            if(date == nil) date = [NSDate date];

            JSQMessage *message = [[JSQMessage alloc] initWithText:text sender:uid date:date];

            [messages insertObject:message atIndex:i++];

            NSString *image = [snapshot.value objectForKey:@"image"];
            NSString *name = [snapshot.value objectForKey:@"name"];

            [users addObject:@{@"name" : name, @"image" : image}];
            avatars[uid] = image;

            if(i > MAX_NUM_EARLIER_MSG_LOAD) {
                [self.collectionView reloadData];
                
                [hud hide:YES];
            }
        }];

        startingPrio -= MAX_NUM_EARLIER_MSG_LOAD + 1;
    }
}

- (void)hudWasHidden:(MBProgressHUD *) hud1 {
    [hud1 removeFromSuperview];
}


- (UICollectionReusableView *)collectionView:(JSQMessagesCollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    if (self.showLoadEarlierMessagesHeader && [kind isEqualToString:UICollectionElementKindSectionHeader]) {
        JSQMessagesLoadEarlierHeaderView *header = [collectionView dequeueLoadEarlierMessagesViewHeaderForIndexPath:indexPath];

        // Customize header
        [header.loadButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        header.loadButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:10];

        return header;
    }

    return [super collectionView:collectionView
            viewForSupplementaryElementOfKind:kind
            atIndexPath:indexPath];
}

- (void)viewWillUnload {
    [[UserProfile sharedInstance] setLastMsgId:msgId Key:chatroom];
}

- (void)viewWillDisappear:(BOOL) animated {
    [[UserProfile sharedInstance] setLastMsgId:msgId Key:chatroom];
}

-(void) appWillResignActive:(NSNotification*) note {
    [[UserProfile sharedInstance] setLastMsgId:msgId Key:chatroom];
}

-(void)appWillTerminate:(NSNotification*) note {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
}

-(void) setObservers{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
