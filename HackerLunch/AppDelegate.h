//
//  AppDelegate.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 6/30/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationTracker.h"
#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property LocationTracker *locationTracker;
@property (nonatomic) NSTimer *locationUpdateTimer;

- (void)updateLocation;
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;
@end
