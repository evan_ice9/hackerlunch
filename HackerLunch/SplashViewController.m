//
//  SplashViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/19/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "SplashViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toHome:) name:FIREBASE_AUTH object:nil];
}

- (void)toHome:(id) toHome {
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    [self performSegueWithIdentifier:@"toHome" sender:nil];
}

-(void)viewDidAppear:(BOOL) animated {
    PFUser* user = [PFUser currentUser];
    if(user){
        [user refreshInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if(error){
                NSLog(@"Error in User: %@", error);
                
                //[NSUserDefaults setUserValue:@NO forKey:kLoggedIn];
                [[UserProfile sharedInstance] setUserObject:@NO forKey:kLoggedIn];
                
                
                [self performSegueWithIdentifier:@"toLogin" sender:nil];

                [[UserProfile sharedInstance] resetUserInfo];
                [PFUser logOut];
            } else{

                [[UserProfile sharedInstance] setUserObject:@YES forKey:kLoggedIn];
                
                //[self performSegueWithIdentifier:@"toHome" sender:nil];
                [[UserProfile sharedInstance] checkAndSetUserName];
            }

        }];
    } else {
        printLog(@"No logged in user found.");

        //[NSUserDefaults setUserValue:@NO forKey:kLoggedIn];
        [[UserProfile sharedInstance] setUserObject:@NO forKey:kLoggedIn];

        [self performSegueWithIdentifier:@"toLogin" sender:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
