//
//  MergeSettingsViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/17/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "MergeSettingsViewController.h"
#import "PairViewController.h"

@interface MergeSettingsViewController ()

@end

@implementation MergeSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    HomeViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"homeView"];
    self.topViewController = vc;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logOutUser:) name:kLogOutUser object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatView:) name:LAUNCH_CHAT_VIEW object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(profileView:) name:LAUNCH_PROFILE_VIEW object:nil];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentPairView:) name:kPairUserController object:nil];
}

- (void)chatView:(NSNotification *) not {
    NSDictionary *info = not.userInfo;

    PFUser *user = (PFUser *) not.object;

    ChatViewHolderViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"chatHolderView"];

    cvc.roomTitle = user[@"fullName"];
    cvc.pUser = user;
    cvc.chatroom = info[@"chatroom"];

    [self presentViewController:cvc animated:YES completion:nil];
}

- (void)profileView:(NSNotification *) not {
    PFUser *user = (PFUser *) not.object;

    ProfileViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
    cvc.user = user;
    cvc.userImage = nil;

    [self presentViewController:cvc animated:YES completion:nil];
}

- (void)viewWillUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *) segue sender:(id) sender {
}

- (void)logOutUser:(NSNotification *) not {
    Firebase* myRef = [[Firebase alloc] initWithUrl:FIREBASE];
    FirebaseSimpleLogin* authClient = [[FirebaseSimpleLogin alloc] initWithRef:myRef];

    [[UserProfile sharedInstance] resetUserInfo];
    [[UserProfile sharedInstance] unregisterForPush];

    [authClient logout];
    [PFUser logOut];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
