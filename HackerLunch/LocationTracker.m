//
//  LocationTracker.m
//  Location
//
//  Created by Rick
//  Copyright (c) 2014 Location All rights reserved.
//

#import "LocationTracker.h"

#define LATITUDE @"latitude"
#define LONGITUDE @"longitude"
#define ACCURACY @"theAccuracy"

@implementation LocationTracker {
  //  BOOL didOnce;
}


+ (CLLocationManager *)sharedLocationManager {
	static CLLocationManager *_locationManager;
	
	@synchronized(self) {
		if (_locationManager == nil) {
			_locationManager = [[CLLocationManager alloc] init];
            //_locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
		}
	}
	return _locationManager;
}

- (id)init {
	if (self==[super init]) {
        //Get the share model and also initialize myLocationArray
        self.shareModel = [LocationShareModel sharedModel];
        self.shareModel.myLocationArray = [[NSMutableArray alloc]init];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
	}
	return self;
}

-(void) authorizeLocation:(CLLocationManager *) locationManager {
    BOOL IS_OS_8_OR_LATER = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0);
    
    if(IS_OS_8_OR_LATER) {
        [locationManager requestAlwaysAuthorization];
    }
}

-(void)applicationEnterBackground{
    NSLog(@"applicationEnterBackground");

    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    locationManager.delegate = self;
    //locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    
    [self authorizeLocation:locationManager];
    [locationManager startUpdatingLocation];

    //Use the BackgroundTaskManager to manage all the background Task
    self.shareModel.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];
}

- (void) restartLocationUpdates
{
    NSLog(@"restartLocationUpdates");

    if (self.shareModel.timer) {
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
    }

    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    
    [self authorizeLocation:locationManager];
    [locationManager startUpdatingLocation];
}


- (void)startLocationTracking {
    NSLog(@"startLocationTracking");

	if (![CLLocationManager locationServicesEnabled]) {
        NSLog(@"locationServicesEnabled false");
		UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[servicesDisabledAlert show];
	} else {
        CLAuthorizationStatus authorizationStatus= [CLLocationManager authorizationStatus];

        if(authorizationStatus == kCLAuthorizationStatusDenied || authorizationStatus == kCLAuthorizationStatusRestricted){
            NSLog(@"authorizationStatus failed");
        } else {
            NSLog(@"authorizationStatus authorized");
            CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            
            [self authorizeLocation:locationManager];
            [locationManager startUpdatingLocation];
        }
	}
}


- (void)stopLocationTracking {
    NSLog(@"stopLocationTracking");
    
    if (self.shareModel.timer) {
        [self.shareModel.timer invalidate];
        self.shareModel.timer = nil;
    }
    
	CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    
    [self authorizeLocation:locationManager];
	[locationManager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate Methods

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
  //  NSLog(@"locationManager didUpdateLocations");
    
    for(int i=0;i<locations.count;i++){
        CLLocation * newLocation = [locations objectAtIndex:i];
        CLLocationCoordinate2D theLocation = newLocation.coordinate;
        CLLocationAccuracy theAccuracy = newLocation.horizontalAccuracy;
        
        NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
        
        if (locationAge > 30.0){
            continue;
        }

      //  NSLog(@"Accuracy %f", theAccuracy);

        //Select only valid location and also location with good accuracy
        if(newLocation != nil && theAccuracy > 0
           && theAccuracy < 2000
           && (!(theLocation.latitude == 0.0 && theLocation.longitude == 0.0))){
            
            self.myLastLocation = theLocation;
            self.myLastLocationAccuracy= theAccuracy;
            
            NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
            dict[@"latitude"] = @(theLocation.latitude);
            dict[@"longitude"] = @(theLocation.longitude);
            dict[@"theAccuracy"] = @(theAccuracy);
            
            //Add the vallid location with good accuracy into an array
            //Every 1 minute, I will select the best location based on accuracy and send to server
            [self.shareModel.myLocationArray addObject:dict];
            
//            if(!didOnce){
//                [self prepareLocationForServer];
//                didOnce = YES;
//            }
        }
    }
    
    //If the timer still valid, return it (Will not run the code below)
    if (self.shareModel.timer) {
        return;
    }
    
    self.shareModel.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];
    
    //Restart the locationMaanger after 1 minute
    self.shareModel.timer = [NSTimer scheduledTimerWithTimeInterval:60 target:self
                                                           selector:@selector(restartLocationUpdates)
                                                           userInfo:nil
                                                            repeats:NO];
    
    //Will only stop the locationManager after 10 seconds, so that we can get some accurate locations
    //The location manager will only operate for 10 seconds to save battery
    NSTimer * delay10Seconds;
    delay10Seconds = [NSTimer scheduledTimerWithTimeInterval:10 target:self
                                                    selector:@selector(stopLocationDelayBy10Seconds)
                                                    userInfo:nil
                                                     repeats:NO];

}


//Stop the locationManager
-(void)stopLocationDelayBy10Seconds{
    CLLocationManager *locationManager = [LocationTracker sharedLocationManager];
    [locationManager stopUpdatingLocation];
    
    NSLog(@"locationManager stop Updating after 10 seconds");
}


- (void)locationManager: (CLLocationManager *)manager didFailWithError: (NSError *)error
{
   // NSLog(@"locationManager error:%@",error);
    
    switch([error code])
    {
        case kCLErrorNetwork: // general, network-related error
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error" message:@"Please check your network connection." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
        case kCLErrorDenied:{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enable Location Service" message:@"You have to enable the Location Service to use this App. To enable, please go to Settings->Privacy->Location Services" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
        default:
        {
            
        }
            break;
    }
}


//Send the location to Server
- (void)prepareLocationForServer {
    
    NSLog(@"prepareLocationForServer");
    
    // Find the best location from the array based on accuracy
    NSMutableDictionary * myBestLocation = [[NSMutableDictionary alloc]init];
    
    for(uint i=0;i<self.shareModel.myLocationArray.count;i++){
        NSMutableDictionary * currentLocation = (self.shareModel.myLocationArray)[i];
        
        if(i==0)
            myBestLocation = currentLocation;
        else{
            if([currentLocation[ACCURACY] floatValue]<=[myBestLocation[ACCURACY] floatValue]){
                myBestLocation = currentLocation;
            }
        }
    }
    //NSLog(@"My Best location:%@",myBestLocation);
    
    //If the array is 0, get the last location
    //Sometimes due to network issue or unknown reason, you could not get the location during that  period, the best you can do is sending the last known location to the server
    if(self.shareModel.myLocationArray.count==0)
    {
        NSLog(@"Unable to get location, use the last known location");

        self.myLocation=self.myLastLocation;
        self.myLocationAccuracy=self.myLastLocationAccuracy;
        
    }else{
        CLLocationCoordinate2D theBestLocation;
        theBestLocation.latitude =[myBestLocation[LATITUDE] floatValue];
        theBestLocation.longitude =[myBestLocation[LONGITUDE] floatValue];
        self.myLocation=theBestLocation;
        self.myLocationAccuracy =[myBestLocation[ACCURACY] floatValue];
    }
    
    //NSLog(@"Send to Server: Latitude(%f) Longitude(%f) Accuracy(%f)",self.myLocation.latitude, self.myLocation.longitude,self.myLocationAccuracy);
    
    [self uploadDataToServer];

    //After sending the location to the server successful, remember to clear the current array with the following code. It is to make sure that you clear up old location in the array and add the new locations from locationManager
    [self.shareModel.myLocationArray removeAllObjects];
    self.shareModel.myLocationArray = nil;
    self.shareModel.myLocationArray = [[NSMutableArray alloc] init];
}



-(void) uploadDataToServer {
    BOOL isLoggedIn = [[[UserProfile sharedInstance] userObjectForKey:kLoggedIn] boolValue]; //[[NSUserDefaults valueForKey:kLoggedIn] boolValue];

    PFUser *user = [PFUser currentUser];
    //NSLog(@"isLoggedIn: %i %i", isLoggedIn, [user isAuthenticated]);
    if(isLoggedIn && user) {
        PFQuery *query = [PFQuery queryWithClassName:class_Location];
        [query whereKey:kUser equalTo:user];

        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if(error) {
                NSString *errorString = [error userInfo][@"error"];
                NSLog(@"%@", errorString);
            }


            if(object) {
                object[@"user"] = user;
                object[LATITUDE] = [NSString stringWithFormat:@"%f", self.myLocation.latitude];
                object[LONGITUDE] = [NSString stringWithFormat:@"%f", self.myLocation.longitude];

                PFGeoPoint *geoPoint = [[PFGeoPoint alloc] init];
                geoPoint.latitude = self.myLocation.latitude;
                geoPoint.longitude = self.myLocation.longitude;

#if TARGET_IPHONE_SIMULATOR
                //23.88562393188477, 90.39000701904297
                object[LATITUDE] = [@23.88562393188477 stringValue];
                object[LONGITUDE] = [@90.39000701904297 stringValue];

                geoPoint.latitude = 23.88562393188477;
                geoPoint.longitude = 90.39000701904297;

                NSLog(@"Send to Server: Latitude(%@) Longitude(%@)", object[LATITUDE], object[LONGITUDE]);
#endif
                [object setValue:geoPoint forKey:@"geoPoint"];

                [object saveEventually];

                //[NSUserDefaults setUserValue:object[LATITUDE] forKey:kLATITUDE];
                //[NSUserDefaults setUserValue:object[LONGITUDE] forKey:kLONGITUDE];

                [[UserProfile sharedInstance] setUserObject:object[LATITUDE] forKey:kLATITUDE];
                [[UserProfile sharedInstance] setUserObject:object[LONGITUDE] forKey:kLONGITUDE];

            }
        }];
    }
}


@end
