//
//  UserProfile.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/26/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import <FirebaseSimpleLogin/FirebaseSimpleLogin.h>

@class NSString;

@interface UserProfile : UIViewController

//typedef void(^onComplete)(BOOL);

@property(nonatomic) BOOL runOnce;

- (void)initChatRoom:(PFUser *) pairedUser;

+ (UserProfile *) sharedInstance;
- (NSDictionary *)parseUserData;
- (void)checkAndSetUserName;

- (void)setMaxDistance:(int) val;

- (int)getMaxDistance;

- (UIImage *)getProfilePicture;

- (NSString *)getFullName;

- (NSString *)getFbPicUrl;

- (void)goOffline;

- (BOOL) setUserObject:(id) object forKey:(NSString *) key;

- (void)removeUserObjectForKey:(NSString *) key;

- (id) userObjectForKey:(NSString *) key;

- (void)unregisterForPush;

- (void)registerAtFireBaseWithEmail:(NSString *) email password:(NSString *) password;

- (BOOL)resetUserInfo;

- (void)setLastMsgId:(NSInteger) val Key:(NSString *) key;

- (NSInteger)getLastMsgId:(NSString *) key;

-(void)setUserProfile:(NSDictionary *)data;

//- (NSMutableArray *)getPairedUserList;


- (void)goOnline;
@end
