//
//  HomeViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/11/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "HomeViewController.h"
#import "SettingsTableViewController.h"

#define FIND_VIEW 0
#define PAIR_VIEW 1
#define MIN_DISTANCE 0.5

@interface HomeViewController ()

@end

@implementation HomeViewController {
    NSMutableArray *userIds;
    NSUInteger nextSelectionIndex;
    MBProgressHUD *hud;
    BOOL isViaFb;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)hudWasHidden:(MBProgressHUD *) _hud {
    [_hud removeFromSuperview];
}


- (void)viewDidLoad
{
    [super viewDidLoad];

//    [[UserProfile sharedInstance] checkAndSetUserName];
    
    isViaFb = [[[UserProfile sharedInstance] userObjectForKey:kIsViaFacebook] boolValue];

    nextSelectionIndex = 0;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[SettingsTableViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsView"];
        SettingsTableViewController *svc = (SettingsTableViewController *) self.slidingViewController.underLeftViewController;
        [svc setHomeView:(HomeViewController *) self.slidingViewController.topViewController];
    }

    if (![self.slidingViewController.underRightViewController isKindOfClass:[ChatHistoryViewController class]]) {
        self.slidingViewController.underRightViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"chatHistoryView"];
    }
    
    [self.slidingViewController setAnchorRightRevealAmount:260.0f];
    [self.slidingViewController setAnchorLeftRevealAmount:260.0f];

    hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.labelText = @"Finding a match...";

    userIds = [[NSMutableArray alloc] init];

    //[self updateLocationDataToServer];

    self.dot.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatDot:) name:CHAT_NOTIFICATION object:nil];
}

- (void)chatDot:(NSNotification *) not {
    BOOL isPending = [not.object boolValue];
    self.dot.hidden = !isPending;
}


- (void) viewWillAppear:(BOOL)animated{
   NSData *imageData = [[UserProfile sharedInstance] userObjectForKey:kLoadProPic];
    
    if(imageData){
        UIImage *image = [UIImage imageWithData:imageData];
        [self.propicVIew setImage:image];
    }else{
      //  hud.labelText = @"Loading data...";
      //  [hud show:YES];
      //  [self.view addSubview:hud];

        /*PFUser *user = [PFUser currentUser];
        PFFile *file = user[kUserProfilePicture];
        NSString *url = file.url;*/
        
        NSString *url=[[UserProfile sharedInstance]userObjectForKey:kUserProfilePicture];
        if(url){
            self.propicVIew.imageURL = [[NSURL alloc] initWithString:url];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(picLoadDone:) name:AsyncImageLoadDidFinish object:nil];
        }else if(isViaFb){
            url = [[UserProfile sharedInstance] getFbPicUrl];
            self.propicVIew.imageURL = [[NSURL alloc] initWithString:url];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(picLoadDone:) name:AsyncImageLoadDidFinish object:nil];
        }else{
            //[hud hide:YES];
          //  [hud removeFromSuperview];
        }
    }
}


- (void)updateLocationDataToServer {
    PFQuery *query = [PFQuery queryWithClassName:class_Location];
    [query whereKey:kUser equalTo:[PFUser currentUser]];

    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
    //[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(error) {
            NSString *errorString = [error userInfo][@"error"];
            NSLog(@"%@", errorString);
        }

        if(object){
            //[NSUserDefaults setUserValue:object.objectId forKey:kLocationObjectId];
            NSLog(@"Objectid: %@", object.objectId);
            
            [[UserProfile sharedInstance] setUserObject:object.objectId forKey:kLocationObjectId];

            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            [appDelegate updateLocation];
        }else{
            printLog(@"nothing found");

            PFObject *locObject = [PFObject objectWithClassName:class_Location];
            locObject[kUser] = [PFUser currentUser];

            PFACL *acl = [PFACL ACLWithUser:[PFUser currentUser]];
            [acl setPublicReadAccess:YES];
            [acl setPublicWriteAccess:YES];

            [locObject setACL:acl];

            [locObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *err) {
                if(succeeded) {
                    [self updateLocationDataToServer];
                }

                if(err) {
                    printLog([error userInfo][@"error"]);
                }
            }];
        }

    }];
}

-(void) showAlert:(NSString *) message{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Error" andMessage:message];
    [alertView addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeDestructive handler:nil];
    [alertView show];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)onNavButton:(UIButton *)sender {
    NSInteger tag = sender.tag;
    
    switch(tag){
        case 10:
            [self.slidingViewController anchorTopViewTo:ECRight];
            break;
        case 11:
            [self.slidingViewController anchorTopViewTo:ECLeft];
            break;
        default:
            break;
    }
}

- (void)checkForAlreadyPairedUser {
    [self.view addSubview:hud];
    hud.labelText = @"Checking user...";
    [hud show:YES];

    PFUser *user = [PFUser currentUser];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [formatter stringFromDate:[NSDate date]];

    PFQuery *query = [PFQuery queryWithClassName:class_chatlog];
    [query whereKey:@"userB" equalTo:user];
    [query whereKey:@"date" equalTo:dateStr];

    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        
        //code - 101
        if(error.code == 101){
            [self startFinding];
        }else if(object){
            PFUser *userA = object[@"userA"];
            dictionary[kPairedUser] = userA;

            [userA fetchInBackgroundWithBlock:^(PFObject *obj, NSError *err) {
                dictionary[kUserObject] = obj;

                //[NSUserDefaults setUserValue:object[@"about"] forKey:kMatchedUserData];

                [[UserProfile sharedInstance] setUserObject:obj[@"about"] forKey:kMatchedUserData];

                PFFile *userImageFile = obj[kUserProfilePicture];

                if(userImageFile) {
                    [userImageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *err2) {
                        if(err2) {
                            //[self showAlert:error.userInfo[@"error"]];
                        } else {
                            UIImage *image = [UIImage imageWithData:data];
                            [hud removeFromSuperview];

                            dictionary[kUserImage] = image;
                            [[NSNotificationCenter defaultCenter] postNotificationName:kPairUserController object:nil userInfo:dictionary];
                        }
                    }];
                }else{
                    UIImage *image = [UIImage imageNamed:@"profile_image_placeholder"];
                    [hud removeFromSuperview];

                    dictionary[kUserImage] = image;
                    [[NSNotificationCenter defaultCenter] postNotificationName:kPairUserController object:nil userInfo:dictionary];
                }
            }];
        }else{
            [hud removeFromSuperview];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNoUserFound object:nil userInfo:nil];
        }
    }];
}

- (void) startFinding {
    if(userIds.count > 0){
        [self findMatch:nextSelectionIndex];
        nextSelectionIndex++;
    }else{
        NSLog(@"loading nearby users");
        [self loadNearByUsers];
    }
}

- (IBAction)onPairMe:(UIButton *)sender {
   // [self checkForAlreadyPairedUser];
    [self startFinding];

    [self changeTopViewController:FIND_VIEW];

    //  ChatViewHolderViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"chatHolderView"];
    //  [self presentViewController:cvc animated:YES completion:nil];
}

- (void)loadNearByUsers {
    hud.labelText = @"Fetching nearby users...";
    [self.view addSubview:hud];
    [hud show:YES];

    double maxDistance = (double) [[UserProfile sharedInstance] getMaxDistance];

    PFGeoPoint *geoPoint = [[PFGeoPoint alloc] init];
    geoPoint.latitude = [[[UserProfile sharedInstance] userObjectForKey:kLATITUDE] doubleValue];
    geoPoint.longitude = [[[UserProfile sharedInstance] userObjectForKey:kLONGITUDE] doubleValue];

    PFQuery *query = [PFQuery queryWithClassName:class_Location];
    query.limit = 50;
    //[query whereKey:@"geoPoint" nearGeoPoint:geoPoint withinKilometers:20000];
    [query whereKey:@"geoPoint" nearGeoPoint:geoPoint withinMiles:maxDistance*10000];

    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        //   NSLog(@"%@", objects);

        if(error) {
            [self showAlert:error.userInfo[@"error"]];
        }else {

            for (uint i = 0; i < objects.count; ++i) {
                //  PFGeoPoint *g = [objects[i] objectForKey:@"geoPoint"];
                //  CLLocation *locA = [[CLLocation alloc] initWithLatitude:geoPoint.latitude longitude:geoPoint.longitude];
                //  CLLocation *locB = [[CLLocation alloc] initWithLatitude:g.latitude longitude:g.longitude];

                [userIds addObject:[objects[i] objectForKey:@"user"]];

                //  NSLog(@"%f", [locA distanceFromLocation:locB]);
            }
            // NSLog(@"%@", userIds);
        }

        [hud removeFromSuperview];

        //remove self user as a result of location inconsistency
        [self removeCurrentUser];

        //find match with nearby user
        [self findMatch:nextSelectionIndex];
        nextSelectionIndex++;
    }];
}

- (void)removeCurrentUser {
    for(int i = 0; i < userIds.count; i++){
        PFUser *user = userIds[i];
        PFUser *currentUser = [PFUser currentUser];

        if([user.objectId isEqualToString:currentUser.objectId]){
            [userIds removeObjectAtIndex:i];
            return;
        }
    }
}

- (void)findMatch:(NSUInteger) index {

    [self.view addSubview:hud];
    [hud show:YES];

    if(index < userIds.count) {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];

        PFUser *user = userIds[index];

        dictionary[kPairedUser] = user;

        [user fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {

          //  NSLog(@"%@", object);
            [hud hide:YES];
            [hud removeFromSuperview];

            if(error) {
                [self showAlert:error.userInfo[@"error"]];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNoUserFound object:nil userInfo:nil];
            } else {

                dictionary[kUserObject] = object;

                //[NSUserDefaults setUserValue:object[@"about"] forKey:kMatchedUserData];

                [[UserProfile sharedInstance] setUserObject:object[@"about"] forKey:kMatchedUserData];

                PFFile *userImageFile = object[kUserProfilePicture];

                if(userImageFile) {
                    [userImageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *err) {
                        if(err) {
                            [self showAlert:error.userInfo[@"error"]];
                        } else {
                            UIImage *image = [UIImage imageWithData:data];
                            //[hud removeFromSuperview];

                            dictionary[kUserImage] = image;
                            [[NSNotificationCenter defaultCenter] postNotificationName:kPairUserController object:nil userInfo:dictionary];
                        }
                    }];
                }else{
                    UIImage *image = [UIImage imageNamed:@"profile_image_placeholder"];
                    //[hud removeFromSuperview];

                    dictionary[kUserImage] = image;
                    [[NSNotificationCenter defaultCenter] postNotificationName:kPairUserController object:nil userInfo:dictionary];
                }
            }
        }];
    }else{
        [hud removeFromSuperview];
       // [self showAlert:@"No matches available at this moment"];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNoUserFound object:nil userInfo:nil];
    }
}

-(void) changeTopViewController:(NSInteger) key {
    UIViewController *newVc;
    CGRect frame = self.slidingViewController.topViewController.view.frame;

    switch(key){
        case FIND_VIEW:
            newVc = [self.storyboard instantiateViewControllerWithIdentifier:@"findView"];
            break;
        case PAIR_VIEW:
            break;
        default:break;
    }

    self.slidingViewController.topViewController.view.frame = frame;

    [UIView transitionWithView:self.slidingViewController.topViewController.view.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        self.slidingViewController.topViewController = newVc;
                    }
                    completion:(void (^)(BOOL)) ^{
                        [self.slidingViewController resetTopView];
                    }];
}

- (void)picLoadDone:(NSNotification *) not {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AsyncImageLoadDidFinish object:nil];

    NSData *data = [NSData dataWithData:UIImagePNGRepresentation(self.propicVIew.image)];
    [[UserProfile sharedInstance] setUserObject:data forKey:kLoadProPic];

    /*PFUser *user = [PFUser currentUser];
    PFFile *file = user[kUserProfilePicture];

    if(file == nil){
        PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:data];
        user[kUserProfilePicture] = imageFile;
        [user saveInBackground];
    }*/

    [hud hide:YES];
    [hud removeFromSuperViewOnHide];
}

@end
