//
//  ChatViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/25/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "ChatViewController.h"
#import "ChatViewHolderViewController.h"
#import "JSQMessages.h"
#import <Firebase/Firebase.h>
#import <AsyncImageView/AsyncImageView.h>

@interface ChatViewController : JSQMessagesViewController <MBProgressHUDDelegate>

@property (strong, nonatomic) UIView *infoView;
@property (nonatomic) BOOL inited;

@property(nonatomic, strong) PFUser *pUser;

- (id)initWith:(NSString *)Chatroom Userinfo:(NSDictionary *)Userinfo;

@end
