//
//  PairViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/17/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "MotherViewController.h"
#import "ECSlidingViewController.h"
#import "ChatViewHolderViewController.h"
#import "ProfileViewController.h"

@interface PairViewController : MotherViewController

@property (strong, nonatomic) PFObject *userObject;
@property (strong, nonatomic) PFUser *user;
@property(nonatomic, strong) UIImage *userImage;
@property (strong, nonatomic) IBOutlet UIImageView *picView;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *statView;
@property (strong, nonatomic) IBOutlet UIView *dot;

- (IBAction)onBack:(UIButton *)sender;
- (IBAction)onViewProfile:(id)sender;
- (IBAction)onHaveLunch:(id)sender;

@end
