//
//  SignupViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/4/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IntroViewController.h"
#import "MotherViewController.h"

@interface SignupViewController : MotherViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *tf_name;
@property (weak, nonatomic) IBOutlet UITextField *tf_email;
@property (weak, nonatomic) IBOutlet UITextField *tf_pass;
@property (weak, nonatomic) IBOutlet UITextField *tf_rpass;

- (IBAction)onCreateBtn:(id)sender;
- (IBAction)onSocialBtn:(UIButton *)sender;

@end
