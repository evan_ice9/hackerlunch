//
//  HistoryViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 9/18/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AsyncImageView/AsyncImageView.h>

@interface HistoryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MBProgressHUDDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *userList;
@property (strong, nonatomic) IBOutlet UIButton *errButt;
@property (strong, nonatomic) IBOutlet UILabel *pairInfoLabel;
@property (strong, nonatomic) IBOutlet UIView *dot;

- (IBAction)onNavBtn:(UIButton *)sender;
- (IBAction)onErrButt:(id)sender;

@end
