//
//  ProfileViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/21/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController {
    UIView *viewEmail;
    UIView *viewFb;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.userNameView.text = self.user[@"fullName"];
    self.statView.text = self.user[@"about"];


    if(self.userImage != nil)
        self.profilePic.image = self.userImage;
    else{
        PFFile *file = self.user[kUserProfilePicture];
        
        if(file.url)
            self.profilePic.imageURL = [[NSURL alloc] initWithString:file.url];
    }

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSettingCellTap:)];
    tap.numberOfTapsRequired = 1;
    tap.cancelsTouchesInView = YES;

    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSettingCellTap:)];
    tap2.numberOfTapsRequired = 1;
    tap2.cancelsTouchesInView = YES;

    viewEmail = [self.socialContainer viewWithTag:10];
    [viewEmail addGestureRecognizer:tap];

    self.emailLabel.text = self.user.username;

    NSString *fid = self.user[@"facebookId"];

    if(fid == nil){
        self.fbIcon.hidden = YES;
        self.fbLabel.text = @"No social networks added yet.";
        //self.socialContainer.frame = CGRectMake(0, 309, 320, 95);
    }else{
        viewFb = [self.socialContainer viewWithTag:11];
        [viewFb addGestureRecognizer:tap2];

        self.fbIcon.hidden = NO;
        self.fbLabel.text = @"View Facebook profile.";
      //  self.socialContainer.frame = CGRectMake(0, 263, 320, 95);
    }

}

-(void)viewWillAppear:(BOOL) animated {
    [self switchColorToOrange];
}


- (void)onSettingCellTap:(UITapGestureRecognizer *) tap {
    int tag = tap.view.tag;

    NSString *urlEmail;
    NSString *fburl;

    switch(tag){
        case 10:    //email
            urlEmail = [NSString stringWithFormat:@"mailto:%@", self.user.username];
            urlEmail = [urlEmail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];

            [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: urlEmail]];
            break;
        case 11:    //fb
            //fburl = [NSString stringWithFormat:@"https://www.facebook.com/profile.php?id=%@", self.user[@"facebookId"]];
            fburl = [NSString stringWithFormat:@"https://www.facebook.com/%@", self.user[@"facebookId"]];

            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fburl]];
            break;
        default:break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onHaveLunch:(id)sender {
    ChatViewHolderViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"chatHolderView"];

    cvc.roomTitle = self.user[@"fullName"];
    cvc.pUser = self.user;

    [self presentViewController:cvc animated:YES completion:nil];
}

- (IBAction)onNavBtn:(UIButton *)sender {
//    int tag = sender.tag;
//
//    if(tag == 10){
//        [self.slidingViewController anchorTopViewTo:ECRight];
//    }else if(tag == 11){
//        [self.slidingViewController anchorTopViewTo:ECLeft];
//    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
