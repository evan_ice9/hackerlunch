//
//  IntroViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/5/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "IntroViewController.h"
#import "UIImage+CustomMethods.h"

#define BTN_FB_TAG 10
#define BTN_TW_TAG 11
#define BTN_CA_TAG 20
#define BTN_UP_TAG 21
#define BTN_TP_TAG 22


@interface IntroViewController ()

@end

@implementation IntroViewController {
    NSData *imageData;
    BOOL isDone;
    BOOL isIdLoaded;
    MBProgressHUD *hud;
    NSString *facebookId;
}

@synthesize isViaFacebook, isViaTwitter, markFb, markTw, profilePicView;
@synthesize isIdLoaded;
@synthesize hud;
@synthesize facebookId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toHome) name:FIREBASE_AUTH object:nil];
    [[UserProfile sharedInstance] checkAndSetUserName];

    isDone = NO;
    hud = [[MBProgressHUD alloc] initWithView:self.view];


    if(isViaFacebook){
        markFb.hidden = NO;
        [self fetchFbPic];
    } else if(isViaTwitter){
        markTw.hidden = NO;
    }

    isIdLoaded = NO;

}

-(void)viewWillAppear:(BOOL) animated {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


-(void) fetchFbPic{
    FBSession *fbSession = [PFFacebookUtils session];
    NSString *accessToken = [fbSession accessTokenData].accessToken;
    NSString *url = [NSString stringWithFormat:@"https://graph.facebook.com/me/picture?type=large&return_ssl_resources=1&access_token=%@", accessToken];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageLoadDone:) name:AsyncImageLoadDidFinish object:nil];
    profilePicView.showActivityIndicator = YES;
    profilePicView.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
    profilePicView.imageURL = [NSURL URLWithString:url];

    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        [hud hide:YES];
        
        if(!error) {
            facebookId = [result objectForKey:@"id"];
            isIdLoaded = YES;

           // NSLog(@"%@", result);
        }else{
            [self showAlert:@"Connection Error"];
        }
        
    }];
}

- (void)imageLoadDone:(NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    UIImage *img = (notification.userInfo)[AsyncImageImageKey];
    imageData = UIImageJPEGRepresentation(img, 0.95f);

    NSData *data = [NSData dataWithData:UIImagePNGRepresentation(profilePicView.image)];
    [[UserProfile sharedInstance] setUserObject:data forKey:kLoadProPic];
}

- (IBAction)onBtnPress:(UIButton *)sender {
    NSInteger tag = sender.tag;
    switch (tag){
        case BTN_FB_TAG:
            if(markFb.isHidden) [self linkToFb];
            break;
        case BTN_TW_TAG:
            break;
        case BTN_CA_TAG: // create account
            [self uploadData];
            break;
        case BTN_UP_TAG: //upload photo
            [self pickImageViaCamera:NO];
            break;
        case BTN_TP_TAG: //take photo
            [self pickImageViaCamera:YES];
            break;
        default:break;
    }
}

- (void)linkToFb {
    NSLog(@"linking fb");

    hud.labelText = @"Linking profile...";
    [self.view addSubview:hud];
    hud.delegate = self;
    [hud show:YES];

    PFUser *user = [PFUser currentUser];
    if(![PFFacebookUtils isLinkedWithUser:user]) {
        [PFFacebookUtils linkUser:user permissions:nil block:^(BOOL succeeded, NSError *error) {
            if(error || !succeeded){
                [hud hide:YES];
                NSString *errorString = [error userInfo][@"error"];
                [self showAlert:(error) ? errorString : @"Network Error."];
            }else {
                if (!user) {
                    [hud hide:YES];
                    
                    NSLog(@"Uh oh. The user cancelled the Facebook login.");
                    [self showAlert:@"User cancelled the Facebook login"];

                } else {
                    NSLog(@"facebook linked!");
                    markFb.hidden = NO;
                    [self fetchFbInformation];
                    [self fetchFbPic];
                    [self showAlert:@"Successfully linked." title:@"Congratulation"];
                }
            }
        }];
    } else {
        [hud hide:YES];
        markFb.hidden = NO;

        [self fetchFbInformation];
        [self fetchFbPic];
        [self showAlert:@"Account already linked." title:@"Info"];
    }
}


- (void)fetchFbInformation{
    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *err) {
        [hud show:NO];
        [hud removeFromSuperview];

        if (!err) {
            [[UserProfile sharedInstance] setUserObject:result[@"email"] forKey:kUserEmailForFb];
            [[UserProfile sharedInstance] setUserObject:result[@"id"] forKey:kFacebookId];
            [[UserProfile sharedInstance] setUserObject:@(YES) forKey:kIsViaFacebook];
        }else{
            [self showAlert:err.userInfo[@"error"]];
        }
    }];
}

- (void)pickImageViaCamera:(BOOL) boool {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && boool){
        NSLog(@"no cam, dude!");
        [self showAlert:@"No Camera!"];
        boool = NO;
    }

    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = boool ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *) picker didFinishPickingMediaWithInfo:(NSDictionary *) info {
    // Access the uncropped image from info dictionary
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];

    // Dismiss controller
    [picker dismissViewControllerAnimated:YES completion:nil];

    float iw = image.size.width;
    float ih = image.size.height;
    float ratio = iw/ih;
    float unitSize = 300;

    // Resize image
    UIGraphicsBeginImageContext(CGSizeMake(unitSize*ratio, unitSize));
    [image drawInRect: CGRectMake(0, 0, unitSize*ratio, unitSize)];
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    smallImage = [smallImage crop:CGRectMake((CGFloat) (unitSize * ratio * 0.5 - unitSize * 0.5), 0, unitSize, unitSize)];

    // Upload image
    imageData = UIImageJPEGRepresentation(smallImage, 0.95f);
    self.profilePicView.image = smallImage;
}

- (void)uploadData {
    if(self.tvIntro.text.length > 0 || imageData != nil){

       // MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
        hud.labelText = @"Preparing profile...";
        [self.view addSubview:hud];
        hud.delegate = self;
        [hud show:YES];

        PFUser *user = [PFUser currentUser];
        NSString *fullname = [[UserProfile sharedInstance] userObjectForKey:kUserFullName]; //[NSUserDefaults valueForKey:kUserFullName];

        if(user.email == nil){
            user.email = user.username;
        }

        if(user[@"fullName"] == nil)
            [user setObject:fullname forKey:@"fullName"];

        if(facebookId)
            [user setObject:facebookId forKey:@"facebookId"];

        if(self.tvIntro.text.length > 0){
            NSLog(@"about me added");
            [user setObject:self.tvIntro.text forKey:@"about"];
        }

        if(imageData != nil){
            hud.mode = MBProgressHUDModeAnnularDeterminate;

            PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:imageData];

            [imageFile saveInBackgroundWithBlock:^(BOOL success, NSError *err) {
                if(success) {
                    [user setObject:imageFile forKey:kUserProfilePicture];

                    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {

                        if(error || !succeeded) {
                            NSString *errorString = [error userInfo][@"error"];
                            [self showAlert:(error) ? errorString : @"Network Error."];
                        } else {
                            NSLog(@"done uploading image");

                            hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                            hud.mode = MBProgressHUDModeCustomView;
                            hud.labelText = @"Done";
                            [hud hide:YES afterDelay:0.5];
                            
                            isDone = YES;

                            //[NSUserDefaults setUserValue:imageData forKey:kLoadProPic];
                            [[UserProfile sharedInstance] setUserObject:imageData forKey:kLoadProPic];
                        }
                    }];

                } else {
                    [hud hide:YES];
                    NSString *errorString = [err userInfo][@"error"];
                    [self showAlert:(err) ? errorString : @"Network Error."];
                }

            } progressBlock:^(int percentDone) {
                hud.progress = (float) percentDone / 100;
            }];
        }else{
            [NSUserDefaults removeUserValueForKey:kLoadProPic];

//            hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
//            hud.mode = MBProgressHUDModeCustomView;
//            hud.labelText = @"Done";
//            [hud hide:YES afterDelay:0.5];

//            isDone = YES;

            [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {

                if(error || !succeeded) {
                    NSString *errorString = [error userInfo][@"error"];
                    [self showAlert:(error) ? errorString : @"Network Error."];
                } else {
                    NSLog(@"done uploading image");

                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                    hud.mode = MBProgressHUDModeCustomView;
                    hud.labelText = @"Done";
                    [hud hide:YES afterDelay:0.5];

                    isDone = YES;
                }
            }];
        }
    } else {
        [self toHome];
    }
}

- (void)toHome {
    //[self performSegueWithIdentifier:@"toHome" sender:nil];

    [self dismissViewControllerAnimated:NO completion:^(void){
        [[NSNotificationCenter defaultCenter] postNotificationName:INTRO_NEXTVIEW object:nil userInfo:nil];
    }];
}

- (void)hudWasHidden:(MBProgressHUD *) hud {
    NSLog(@"bye bye");
    [hud removeFromSuperview];

    if(isDone) {
        [self toHome];
    }
}

-(void) showAlert:(NSString *) message {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Error" andMessage:message];
    [alertView addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeDestructive handler:nil];
    [alertView show];
}

-(void) showAlert:(NSString *) message title:(NSString *) title {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:title andMessage:message];
    [alertView addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeDestructive handler:nil];
    [alertView show];
}

- (void)textViewDidBeginEditing:(UITextView *) textView {
    [self.tfIntroBg setPlaceholder:@""];
}

- (BOOL)textViewShouldEndEditing:(UITextView *) textView {
    [self.view endEditing:YES];
    return YES;
}

- (void)touchesEnded:(NSSet *) touches withEvent:(UIEvent *) event {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
