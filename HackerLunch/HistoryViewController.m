//
//  HistoryViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 9/18/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <APUtils/UIColor+APUtils.h>
#import "HistoryViewController.h"
#import "ECSlidingViewController.h"

@interface HistoryViewController ()

@end

@implementation HistoryViewController {
    MBProgressHUD *hud;
    NSMutableArray *dateList;
}

@synthesize userList;

- (void)viewDidLoad {
    [super viewDidLoad];

    userList = [[NSMutableArray alloc] init];
    dateList = [[NSMutableArray alloc] init];

    hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.labelText = @"Fetching data...";
    [self.view addSubview:hud];
    [hud show:YES];

  //  [self.tableView setTableFooterView:[UIView new]];

    [self getPairedUserList];

    self.dot.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatDot:) name:CHAT_NOTIFICATION object:nil];
}

- (void)chatDot:(NSNotification *) not {
    bool isPending = (bool) not.object;
    self.dot.hidden = !isPending;
}

- (void)getPairedUserList {
    PFUser *user = [PFUser currentUser];

    PFQuery *query1 = [PFQuery queryWithClassName:class_chatlog];

    [query1 whereKey:@"channelName" containsString:user.objectId];

    //PFQuery *query = [PFQuery orQueryWithSubqueries:@[query1, query2]];

    [query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {

        if(error){
            self.errButt.hidden = NO;
            self.errButt.userInteractionEnabled = YES;
        }

        if(objects.count > 0){

            NSMutableArray *array = [[NSMutableArray alloc] init];

            self.pairInfoLabel.hidden = YES;

            [dateList removeAllObjects];

            for(PFObject *p in objects){
                PFUser *u = p[@"userA"];

                [dateList addObject:p.createdAt];

                if([u.objectId isEqualToString:user.objectId]){
                    u = p[@"userB"];
                }

                PFQuery *q = [PFUser query];
                [q whereKey:@"objectId" equalTo:u.objectId];

                [array addObject:q];
            }

            PFQuery *query2  = [PFQuery orQueryWithSubqueries:array];

            [query2 findObjectsInBackgroundWithBlock:^(NSArray *objs, NSError *err) {

                [hud hide:YES];

                if(err){
                    self.errButt.hidden = NO;
                    self.errButt.userInteractionEnabled = YES;
                }

                if(objects.count > 0){
                    userList = [[NSMutableArray alloc] initWithArray:objs];
                    [self.tableView reloadData];
                }else{
                    self.pairInfoLabel.hidden = NO;
                }

            }];

            [self.tableView reloadData];

        }else{
            self.pairInfoLabel.hidden = NO;
            [hud hide:YES];
        }
    }];
}


- (UITableViewCell *)tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *) indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];

    UILabel *name = (UILabel *) [cell viewWithTag:11];
    UILabel *date = (UILabel *) [cell viewWithTag:12];

    AsyncImageView *img = (AsyncImageView *) [cell viewWithTag:10];

    PFUser *user = userList[indexPath.row];

    if(user[kUserFullName] != nil)
        name.text = user[kUserFullName];

    PFFile *userImageFile = user[kUserProfilePicture];

    NSDate *dat = dateList[indexPath.row];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd"];
    NSString *dateStr = [formatter stringFromDate:dat];

    date.text = [dateStr uppercaseString];

    if(userImageFile.url != nil)
        img.imageURL = [[NSURL alloc] initWithString:userImageFile.url];

    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 51, 280, 0.5)];
    separatorLineView.backgroundColor = [UIColor colorWithHexString:@"DFDFDF"]; // set color as you want.
    [cell.contentView addSubview:separatorLineView];

    return cell;
}

- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger) section {
    return userList.count;
}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    return [UIView new];
//}

- (void)tableView:(UITableView *) tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *) indexPath {
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithHexString:@"F9F9F9"];
    [cell setSelectedBackgroundView:bgColorView];
}

- (void)tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *) indexPath {
    [[NSNotificationCenter defaultCenter] postNotificationName:LAUNCH_PROFILE_VIEW object:userList[indexPath.row] userInfo:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNavBtn:(UIButton *)sender {
    int tag = sender.tag;

    if(tag == 10){
        [self.slidingViewController anchorTopViewTo:ECRight];
    }else if(tag == 11){
        [self.slidingViewController anchorTopViewTo:ECLeft];
    }
}

- (IBAction)onErrButt:(id)sender {
    self.errButt.hidden = YES;
    self.errButt.userInteractionEnabled = NO;

    [self getPairedUserList];
}

- (void)hudWasHidden:(MBProgressHUD *) mHud {
    [mHud removeFromSuperview];
}
@end
