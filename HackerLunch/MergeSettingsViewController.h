//
//  MergeSettingsViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/17/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import "HomeViewController.h"
#import <FirebaseSimpleLogin/FirebaseSimpleLogin.h>

@interface MergeSettingsViewController : ECSlidingViewController

@property (strong, nonatomic) IBOutlet UIView *viewHolder;

@end
