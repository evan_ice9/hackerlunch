//
//  ChatViewHolderViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/25/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "MotherViewController.h"
#import "ChatViewController.h"

@interface ChatViewHolderViewController : MotherViewController


@property (strong, nonatomic) NSString* chatroom;
@property (strong, nonatomic) NSDictionary* userinfo;
@property(nonatomic, strong) PFUser *pUser;
@property(nonatomic, strong) NSString *roomTitle;

@property (strong, nonatomic) IBOutlet UILabel *oppName;

@property (strong, nonatomic) IBOutlet UIView *chatHolder;

@property (strong, nonatomic) IBOutlet UIView *infoView;

- (IBAction)onNavBtn:(UIButton *)sender;

@end
