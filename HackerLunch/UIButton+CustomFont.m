//
//  UIButton+CustomFont.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/4/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "UIButton+CustomFont.h"

@implementation UIButton (CustomFont)

- (NSString *) fontName {
    return self.titleLabel.font.fontName;
}

- (void) setFontName:(NSString *)fontName {
    self.titleLabel.font = [UIFont fontWithName:fontName size:self.titleLabel.font.pointSize];
}

- (NSString *) cornerRadiusSize {
    return self.cornerRadiusSize;
}

- (void) setCornerRadiusSize:(NSString *)cornerRadiusSize {
    self.layer.cornerRadius = [cornerRadiusSize floatValue];
}


@end
