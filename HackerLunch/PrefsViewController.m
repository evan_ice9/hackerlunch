//
//  PrefsViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 9/17/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "PrefsViewController.h"
#import "ECSlidingViewController.h"


@interface PrefsViewController ()

@end

@implementation PrefsViewController {
    MBProgressHUD *hud;
    BOOL lock;
    NSArray *miles;
    NSDictionary *distMap;
    NSInteger currentDistance;
    CSAnimationView *holder;
    UITapGestureRecognizer *cancelView;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    hud = [[MBProgressHUD alloc] initWithView:self.view];

    self.viewInput.layer.borderWidth = 1.0f;
    self.viewInput.layer.borderColor = [UIColor lightGrayColor].CGColor;

    miles = @[@"1 miles", @"2 miles", @"5 miles", @"10 miles"];
    distMap = @{@"0" : @1, @"1" : @2, @"2" : @5, @"3" : @10};
    currentDistance = [[UserProfile sharedInstance] getMaxDistance];

    [self setUserValues];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDistanceTap:)];
    tap.numberOfTapsRequired = 1;
    tap.cancelsTouchesInView = YES;

    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSettingCellTap:)];
    tap2.numberOfTapsRequired = 1;
    tap2.cancelsTouchesInView = YES;

    UITapGestureRecognizer *tap3= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSettingCellTap:)];
    tap3.numberOfTapsRequired = 1;
    tap3.cancelsTouchesInView = YES;

    [self.viewDistance addGestureRecognizer:tap];
    [self.viewEmail addGestureRecognizer:tap2];
    [self.viewPassword addGestureRecognizer:tap3];
    
    
}

- (void)setUserValues {
    int dist = [[[UserProfile sharedInstance] userObjectForKey:kMaxDistance] intValue];
    UILabel *label = (UILabel *) [self.viewDistance viewWithTag:100];

    if(dist == (int) nil){
        label.text = @"2 miles";
    }else{
        label.text = [NSString stringWithFormat:@"%i miles", dist];
    }
    
    
}

- (void)onDistanceTap:(UITapGestureRecognizer *) tap {
    
    
        holder=[[CSAnimationView alloc]initWithFrame:CGRectMake(0,0, 320, 568)];
        holder.layer.borderWidth=1.0f;
        holder.layer.borderColor=[[UIColor grayColor] CGColor];
        [holder setBackgroundColor:[UIColor colorWithWhite:0 alpha:.5]];
    
        int dist = [[[UserProfile sharedInstance] userObjectForKey:kMaxDistance] intValue];
        NSString *selected_value=[NSString stringWithFormat:@"%i miles",dist];
        
        UIPickerView *picker=[[UIPickerView alloc]initWithFrame:CGRectMake(80, 234, 160, 50)];
        [picker setBackgroundColor:[UIColor whiteColor]];
        picker.layer.cornerRadius=5.0;
        picker.dataSource=self;
        picker.delegate=self;
    
        NSLog(@"%@",selected_value);
        if ([miles containsObject:selected_value])
        {
            int index=[miles indexOfObject:selected_value];
            NSLog(@"%d",index);
            [picker selectRow:index inComponent:0 animated:YES];
        }
        
        [holder addSubview:picker];
        [holder setTag:150];
    
        cancelView=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelPickerView:)];
        [holder addGestureRecognizer:cancelView];
    
        
        [self.view addSubview:holder];
        
        
        NSLog(@"select Distance");
        holder.duration=0.5;
        holder.type=CSAnimationTypeFadeIn;
        [holder startCanvasAnimation];


   /* [ActionSheetStringPicker showPickerWithTitle:@"Select a distance"
                                            rows:miles
                                initialSelection:0//[[distMap allKeysForObject:@(currentDistance)][0] integerValue]
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           currentDistance = [distMap[@(selectedIndex)] integerValue];
                                           [[UserProfile sharedInstance] setUserObject:@(currentDistance) forKey:kMaxDistance];
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                     }
                                          origin:self.view];*/
}


- (void)onSettingCellTap:(UITapGestureRecognizer *) tap {
    int tag = tap.view.tag;

    if(lock) tag = 100;
    
    NSLog(@"%d",tag);

    SIAlertView *alertView = [[SIAlertView alloc] init];
    alertView.alertViewStyle = SIAlertViewStyleTextInput;
    
    switch(tag){
        case 10:    //distance



//            self.distanceTitle.hidden = NO;
//            self.viewInput.type = CSAnimationTypeFadeIn;
//            [self.viewInput startCanvasAnimation];
//            lock = YES;
//
//            self.tfInput.secureTextEntry = NO;
//            self.tfInput.text = @"";
//            self.tfInput.placeholder = @"";

            break;
        case 11:    //email
            self.viewInput.type = CSAnimationTypeFadeIn;
            [self.viewInput startCanvasAnimation];
            lock = YES;

            self.tfInput.secureTextEntry = NO;
            self.tfInput.text = @"";
            self.tfInput.placeholder = @"New email address";
            break;
        case 12:    //pass
            self.viewInput.type = CSAnimationTypeFadeIn;
            [self.viewInput startCanvasAnimation];
            lock = YES;

            self.tfInput.secureTextEntry = YES;
            self.tfInput.text = @"";
            self.tfInput.placeholder = @"New password";
            break;
        default:break;
    }
}

- (void)changeInfo:(NSInteger) x {
    hud.labelText = @"Applying...";
    [hud show:YES];
    [self.view addSubview:hud];

    PFUser *user = [PFUser currentUser];

    if(x == 0){
        user.email = self.tfInput.text;

        [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [hud hide:YES];
            [hud removeFromSuperview];

            if(error){
                [self showAlert:error.userInfo[@"error"]];
            }else if(succeeded){
                //[[UserProfile sharedInstance] setUserObject:self.tfInput.text forKey:kUserEmail];

                self.viewInput.type = CSAnimationTypeFadeOut;
                [self.viewInput startCanvasAnimation];
                lock = NO;

            }else{
                [self showAlert:@"Something went wrong."];
            }
        }];

    }else if(x == 1){
        user.password = self.tfInput.text;

        [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [hud hide:YES];
            [hud removeFromSuperview];

            if(error){
                [self showAlert:error.userInfo[@"error"]];
            }else if(succeeded){
                self.viewInput.type = CSAnimationTypeFadeOut;
                [self.viewInput startCanvasAnimation];
                lock = NO;

            }else{
                [self showAlert:@"Something went wrong."];
            }
        }];
    }
}

//- (void)showInputAlert:(NSNotification *) noti {
//    NSInteger val = [noti.object integerValue];
//
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reopenAlert" object:nil];
//
//    UILabel *label = (UILabel *) [self.viewDistance viewWithTag:100];
//
//    if(val == 0){
//        label.text = @"invalid";
//    }else{
//        label.text = [NSString stringWithFormat:@"%i", val];
//        [[UserProfile sharedInstance] setUserObject:@(val) forKey:kMaxDistance];
//    }
//
//}

- (IBAction)onButton:(UIButton *)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (IBAction)onNotificationSwitch:(UISwitch *)sender {
    [sender setOn:[sender isOn] animated:YES];

    if([sender isOn]){
        NSLog(@"on");
    }else {
        NSLog(@"off");
    }
}

- (IBAction)onInputButton:(UIButton *)sender {
    int tag = sender.tag;

    if(tag == 10){
        if(!self.distanceTitle.hidden){
            NSString *val = self.tfInput.text;

            if([val matches:kIntegerRegex]){
                int num = [val integerValue];
                NSLog(@"wrong");

                if(num >= 1 && num < 101){
                    //[[NSNotificationCenter defaultCenter] postNotificationName:@"reopenAlert" object:@(num) userInfo:nil];
                    //success

                    UILabel *label = (UILabel *) [self.viewDistance viewWithTag:100];
                    label.text = [NSString stringWithFormat:@"%i", num];
                    self.distanceTitle.hidden = YES;

                    self.viewInput.type = CSAnimationTypeFadeOut;
                    [self.viewInput startCanvasAnimation];
                    lock = NO;

                    [[UserProfile sharedInstance] setUserObject:@(num) forKey:kMaxDistance];
                }else{
                    [self showAlert:@"Wrong input distance."];
                }
            }else{
                //[[NSNotificationCenter defaultCenter] postNotificationName:@"reopenAlert" object:@(NO) userInfo:nil];
                [self showAlert:@"Wrong input distance."];
            }
        }else{
            [self checkLoginData];
        }

    }else if(tag == 11){
        self.viewInput.type = CSAnimationTypeFadeOut;
        [self.viewInput startCanvasAnimation];
        NSLog(@"hello");

        self.distanceTitle.hidden = YES;

        lock = NO;
    }
}

- (void)checkLoginData {
    NSString *txt = [self.tfInput text];

    if(!self.tfInput.secureTextEntry) {
        if(![txt matches:kEmailRegex]) {
            [self showAlert:@"Invalid email address."];
        }else {
            [self changeInfo:0];
        }
    }else {
        if(txt.length == 0) {
            [self showAlert:@"Invalid password."];
        }else{
            [self changeInfo:1];
        }
    }
     //   [self beingLoginProcessWithEmail:txt password:pass];
}



- (void)touchesEnded:(NSSet *) touches withEvent:(UIEvent *) event {
    [self.view endEditing:YES];
}


-(void) showAlert:(NSString *) message{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Error" andMessage:message];
    [alertView addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeDestructive handler:nil];
    [alertView show];
}

#pragma mark pickerview-delegates

-(int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    
    return miles.count;
}

-(int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return [miles objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSString *distance_text=[miles objectAtIndex:row];
    holder.type=CSAnimationTypeFadeOut;
    
    currentDistance = [[distMap objectForKey:[NSString stringWithFormat:@"%d",row]] integerValue];
    NSLog(@"%d",currentDistance);
    [[UserProfile sharedInstance] setUserObject:@(currentDistance) forKey:kMaxDistance];
    
    
    UILabel *distance_value=(UILabel *)[self.viewDistance viewWithTag:100];
    [distance_value setText:distance_text];
    
    holder.duration=0.5;
    [holder startCanvasAnimation];
    
    


}

-(void)cancelPickerView:(UITapGestureRecognizer *)tap
{
    holder.type=CSAnimationTypeFadeOut;
    holder.duration=0.5;
    [holder startCanvasAnimation];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
