//
//  SignupViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/4/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>
#import "SignupViewController.h"

#define BTN_FB_TAG 10
#define BTN_TW_TAG 11

@interface SignupViewController ()

@end

@implementation SignupViewController

@synthesize tf_email, tf_name, tf_pass, tf_rpass;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    tf_name.delegate = self;
    tf_email.delegate = self;
    tf_pass.delegate = self;
    tf_rpass.delegate = self;
}

- (void)prepareForSegue:(UIStoryboardSegue *) segue sender:(id) sender {
    [super prepareForSegue:segue sender:sender];

    if([segue.destinationViewController isKindOfClass:IntroViewController.class]) {
        IntroViewController *ivc = (IntroViewController *) segue.destinationViewController;

        NSString *val = (NSString *) sender;
        ivc.isViaFacebook = [val isEqualToString:@"fb"];
        ivc.isViaTwitter = [val isEqualToString:@"tw"];

        //[NSUserDefaults setUserValue:@([val isEqualToString:@"fb"]) forKey:kIsViaFacebook];
        [[UserProfile sharedInstance] setUserObject:@([val isEqualToString:@"fb"]) forKey:kIsViaFacebook];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *) textField {
    if(textField == tf_name){
        [tf_email becomeFirstResponder];
    }if(textField == tf_email){
        [tf_pass becomeFirstResponder];
    }if(textField == tf_pass){
        [tf_rpass becomeFirstResponder];
    }else if(textField == tf_rpass){
        [self.view endEditing:YES];
        [self checkInputDataAndSignup];
    }

    return YES;
}

- (void)checkInputDataAndSignup {
    NSString *name = [tf_name text];
    NSString *email = [tf_email text];
    NSString *pass = [tf_pass text];
    NSString *rpass = [tf_rpass text];

    if(name.length < 2){
        [self showAlert:@"Invalid name."];
    }else if(![email matches:kEmailRegex]){
        [self showAlert:@"Invalid email address."];
    }else if(pass.length == 0){
        [self showAlert:@"Invalid password."];
    }else if(![pass isEqualToString:rpass]){
        [self showAlert:@"Passwords mismatched."];
    }else {

        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
        hud.labelText = @"Siging up...";
        [hud show:YES];
        [self.view addSubview:hud];

        PFUser *newUser = [[PFUser alloc] init];
        [newUser setObject:name forKey:@"fullName"];
        newUser.username = email;
        newUser.email = email;
        newUser.password = pass;

        [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [hud show:NO];
            [hud removeFromSuperview];

            if(error || !succeeded){
                NSString *errorString = [error userInfo][@"error"];
                if(error.code == 202){
                    NSString *str = [NSString stringWithFormat:@"The email address %@ is already registered.", email];
                    [self showAlert:str];
                }else [self showAlert:(error) ? errorString : @"Network Error."];
            }else{
                NSLog(@"Sign up Success");

                //[NSUserDefaults setUserValue:name forKey:kUserFullName];
                [[UserProfile sharedInstance] setUserObject:name forKey:kUserFullName];
                
                //[self performSegueWithIdentifier:@"toIntro" sender:nil];
                [self toNextView:nil];
            }
        }];
    }
}

-(void) showAlert:(NSString *) message{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Error" andMessage:message];
    [alertView addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeDestructive handler:nil];
    [alertView show];
}

- (IBAction)onLogout:(id)sender {
    [PFUser logOut];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onCreateBtn:(id)sender {
    [self checkInputDataAndSignup];
}

- (IBAction)onSocialBtn:(UIButton *) sender {
    if(sender.tag == BTN_FB_TAG){
        [self loginWithFb];
    } else if(sender.tag == BTN_TW_TAG){

    }
}

- (void)loginWithFb {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];

    hud.labelText = @"Connecting to Facebook...";
    [hud show:YES];

    NSArray *permissionsArray = @[@"email"];
    //NSArray *permissionsArray = @[@"user_about_me", @"user_relationships", @"user_birthday", @"user_location"];

    // Login PFUser using facebook

    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        [hud show:NO];
        [hud removeFromSuperview];

        if (!user) {
            if (!error) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
                [self showAlert:@"User cancelled the Facebook login"];
            } else {
                NSLog(@"Uh oh. An error occurred: %@", error);
                [self showAlert:@"Something's not quite right..."];
            }
        } else {
            NSLog(@"User with facebook logged in!");

            //[NSUserDefaults setUserValue:user.objectId forKey:kUserObjectId];
            [[UserProfile sharedInstance] setUserObject:user.objectId forKey:kUserObjectId];

            [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *err) {
                [hud show:NO];
                [hud removeFromSuperview];

                if (!error) {
                    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Congratulation!" andMessage:@"Successfully Logged In"];
                    [alertView addButtonWithTitle:@"Continue" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView * alertView1){
                        [self.tf_pass setText:@""];
                        [self toNextView:@"fb"];
                    }];

                   // [NSUserDefaults setUserValue:result[@"email"] forKey:kUserEmailForFb];
                   // [NSUserDefaults setUserValue:result[@"id"] forKey:kFacebookId];

                    [[UserProfile sharedInstance] setUserObject:result[@"email"] forKey:kUserEmailForFb];
                    [[UserProfile sharedInstance] setUserObject:result[@"id"] forKey:kFacebookId];

                    [alertView show];
                }else{
                    NSLog(@"Uh oh. An error occurred: %@", error);
                    [self showAlert:@"Something's not quite right..."];
                }
            }];
        }
    }];
}

- (void)toNextView:(NSString *) socialValue {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:socialValue forKey:@"socialValue"];

    [self dismissViewControllerAnimated:YES completion:^(void){
        [[NSNotificationCenter defaultCenter] postNotificationName:SIGNEDUP_NEXTVIEW object:nil userInfo:dict];
    }];
}

- (void)touchesEnded:(NSSet *) touches withEvent:(UIEvent *) event {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
