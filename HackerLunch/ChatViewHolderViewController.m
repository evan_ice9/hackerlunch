//
//  ChatViewHolderViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/25/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "ChatViewHolderViewController.h"
#import "ProfileViewController.h"

@interface ChatViewHolderViewController ()

@end

@implementation ChatViewHolderViewController {
    ChatViewController *chatView;
    BOOL inited;
    PFObject *chatLog;
    BOOL firstTime;
}

@synthesize userinfo, chatroom, oppName;

- (void)viewDidLoad {
    [super viewDidLoad];

    firstTime = YES;
    userinfo = [[UserProfile sharedInstance] parseUserData];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setInited:) name:@"setInited" object:nil];

    oppName.text = self.roomTitle;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTitleTap:)];
    tap.numberOfTapsRequired = 1;
    tap.cancelsTouchesInView = YES;
    [oppName addGestureRecognizer:tap];

    [self initChatRoom];
}

- (void)onTitleTap:(UITapGestureRecognizer *) tap {
    if([self.presentingViewController isKindOfClass:ProfileViewController.class]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        ProfileViewController *cvc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
        cvc.user = self.pUser;
        cvc.userImage = nil;

        [self presentViewController:cvc animated:YES completion:nil];
    }
}

- (void)viewWillDisappear:(BOOL) animated {
    NSInteger val = [[UserProfile sharedInstance] getLastMsgId:chatLog[@"channelName"]];
    if(val == (NSInteger) nil) val = -1;
    chatLog[@"lastMessageId"] = @(val);
    [chatLog saveEventually];

    [chatView.view removeFromSuperview];
    chatView = nil;
}

- (void)setInited:(NSNotification *) noti {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"setInited" object:nil];

    chatLog[@"inited"] = @(YES);
    [chatLog saveEventually];

    NSLog(@"set inited");
}

- (void)initChatRoom {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.labelText = @"Establishing connection...";
    [self.view addSubview:hud];
    [hud show:YES];

    chatLog = [PFObject objectWithClassName:class_chatlog];

    PFUser *currentUser = [PFUser currentUser];
    PFUser *pairedUser = self.pUser;
    NSString *channelName = [NSString stringWithFormat:@"%@%@", currentUser.objectId, self.pUser.objectId];

    chatLog[@"userA"] = currentUser;
    chatLog[@"userB"] = pairedUser;
    chatLog[@"channelName"] = channelName;
    chatLog[@"direction"] = currentUser.objectId;

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [formatter stringFromDate:[NSDate date]];

    chatLog[@"date"] = dateStr;

    NSString *str = [NSString stringWithFormat:@"%@%@",currentUser.objectId, pairedUser.objectId];
    NSString *rStr = [NSString stringWithFormat:@"%@%@",pairedUser.objectId, currentUser.objectId];
    
    PFQuery *query1 = [PFQuery queryWithClassName:class_chatlog];
    [query1 whereKey:@"channelName" containsString:str];
    
    PFQuery *query2 = [PFQuery queryWithClassName:class_chatlog];
    [query2 whereKey:@"channelName" containsString:rStr];
    
    PFQuery *query = [PFQuery orQueryWithSubqueries:@[query1, query2]];

    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if(error){
            if(error.code == kPFErrorObjectNotFound){
                //doesn't exist
                chatLog[@"inited"] = @(NO);
                
                inited = NO;

                [chatLog saveInBackgroundWithBlock:^(BOOL succeeded, NSError *err) {
                    [hud hide:YES];
                    [hud removeFromSuperview];

                    chatroom = channelName;

                    firstTime = NO;
                    [self loadChatView];
                }];
            }else{
                [hud hide:YES];
                [hud removeFromSuperview];

                [self showAlert:error.userInfo[@"error"]];
            }
        }else{
            [hud hide:YES];
            [hud removeFromSuperview];

            if(object){
                //already exist
                NSLog(@"%@", object[@"channelName"]);

                chatLog = object;

                inited = [object[@"inited"] boolValue];

                chatroom = object[@"channelName"];

                [self loadChatView];
            }else{

            }
        }

    }];
}

- (void)loadChatView {
    chatView = [[ChatViewController alloc] initWith:chatroom Userinfo:userinfo];
    chatView.view.backgroundColor = [UIColor clearColor];
    chatView.infoView = self.infoView;
    chatView.inited = inited;
    chatView.pUser = self.pUser;

    [self.chatHolder addSubview:chatView.view];
}

-(void) showAlert:(NSString *) message {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Error" andMessage:message];
    [alertView addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeDestructive handler:nil];
    [alertView show];
}

-(void)viewWillAppear:(BOOL) animated {
    [self switchColorToOrange];

    if(!firstTime) [self loadChatView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onNavBtn:(UIButton *)sender {
    //[Firebase goOffline];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
