//
//  ConstantsKeys.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/11/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#ifndef HackerLunch_ConstantsKeys_h
#define HackerLunch_ConstantsKeys_h

#define FIREBASE @"https://hackerlunch.firebaseio.com"
#define HACKERLUNCH_SERVER @"http://hackerlunch.com"


#define kLoadProPic @"loadProPic"
#define kLoggedIn @"loggedIn"

//main info holder
#define kMainDictionary @"userinfo"

//user constants
#define kUser @"user"
#define kUserFullName @"firstname"
#define kUserFirstName @"firstname"
#define kUserLastName @"lastname"
#define kUserEmail @"email"
#define kUserEmailForFb @"fbEmail"
#define kUserProfilePicture @"photo_url"
#define kIsViaFacebook @"user_type"
#define kUserObjectId @"objectId"
#define kFacebookId @"fbId"

#define kLastMessageId @"lastMsgId"

//location constants
#define kLocationObjectId @"objectId"
#define kGEOPOINT @"GEOPOINT"
#define kLATITUDE @"LATITUDE"
#define kLONGITUDE @"LONGITUDE"
#define kLogOutUser @"logoutUser"


//pair constants
#define kPairedUser @"pairedUser"
#define kUserObject @"userObject"
#define kUserImage @"userImage"
#define kPairUserController @"pairUserView"
#define kNoUserFound @"noUser"
#define kMatchedUserData @"matchedUserData"
#define kUserProfile @"userprofile"


#define kMaxDistance @"maxdist"
//#define kMinDistance @"maxdist"

#define class_Location @"LocationData"
#define class_chatlog @"UsersChatLog"


#define SIGNEDUP_NEXTVIEW @"signedupNotfication"
#define INTRO_NEXTVIEW @"introNotfication"
#define LAUNCH_CHAT_VIEW @"launchChatView"
#define LAUNCH_PROFILE_VIEW @"launchProfileView"
#define FIREBASE_AUTH @"firebaseAuth"
#define CHAT_NOTIFICATION @"chatNotification"


#endif
