//
//  MotherViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/5/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "MotherViewController.h"
#import "UIColor+APUtils.h"

@interface MotherViewController ()

@end

@implementation MotherViewController {
    UIView *overlayView_blue;
    UIImageView *bgImageView;
}

@synthesize overlayView_blue;
@synthesize overlayView_orange;
@synthesize bgImageView;

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initBackground];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setBackground:) name:kBackgroundImageLoadedNotification object:nil];
}

- (void)setBackground:(NSNotification *) not {   
    UIImage *image;

    if(not)
        image = (not.userInfo)[kPhotos];
    else image = [BackgroundPhotoSingleton sharedInstance].lastImage;

    [UIView transitionWithView:bgImageView duration:1.0f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        bgImageView.image = image;
        //  self.backgroundPhotoWithImageEffects.image = photos.photoWithEffects;
        //  self.photoUserInfoBarButton.title = title;
    } completion:NULL];
}

- (void)initBackground {
    bgImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    overlayView_blue = [[UIView alloc] initWithFrame:self.view.frame];
    overlayView_orange = [[UIView alloc] initWithFrame:self.view.frame];

    [bgImageView setBackgroundColor:[UIColor clearColor]];
    [overlayView_blue setBackgroundColor:[UIColor colorWithHexString:@"14677a"]];
    [overlayView_orange setBackgroundColor:[UIColor colorWithHexString:@"f3a848"]];
    overlayView_blue.alpha = 0.70f;
    overlayView_orange.alpha = 0.0f;

    [self.view addSubview:bgImageView];
    [self.view addSubview:overlayView_blue];
    //[self.view addSubview:overlayView_orange];

    //[self.view sendSubviewToBack:overlayView_orange];
    [self.view sendSubviewToBack:overlayView_blue];
    [self.view sendSubviewToBack:bgImageView];

    if([BackgroundPhotoSingleton sharedInstance].lastImage)
        [self setBackground:nil];
    
   // NSLog(@"Zz");
}

-(void) switchColorToOrange{
    [overlayView_blue setBackgroundColor:[UIColor colorWithHexString:@"f3a848"]];
    self.view.backgroundColor = [UIColor colorWithHexString:@"f3a848"];
}

-(void) switchColor{
    if(overlayView_orange.alpha <= 0.01) {
        [UIView animateWithDuration:0.5 animations:^{
            overlayView_blue.alpha = 0.0f;
            overlayView_orange.alpha = 0.70f;
            self.view.backgroundColor = [UIColor colorWithHexString:@"f3a848"];
        }];
    }else{
        [UIView animateWithDuration:0.5 animations:^{
            overlayView_blue.alpha = 0.70f;
            overlayView_orange.alpha = 0.0f;
            self.view.backgroundColor = [UIColor colorWithHexString:@"45c8d9"];
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
