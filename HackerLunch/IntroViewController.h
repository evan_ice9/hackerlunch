//
//  IntroViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/5/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
//#import <FacebookSDK/FacebookSDK.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <SIAlertView/SIAlertView.h>
#import "HomeViewController.h"
#import "MotherViewController.h"

@interface IntroViewController : MotherViewController <UITextViewDelegate, MBProgressHUDDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic) BOOL isViaFacebook;
@property (nonatomic) BOOL isViaTwitter;

@property (weak, nonatomic) IBOutlet UIImageView *markFb;
@property (weak, nonatomic) IBOutlet UIImageView *markTw;
@property (weak, nonatomic) IBOutlet AsyncImageView *profilePicView;
@property (weak, nonatomic) IBOutlet UITextView *tvIntro;
@property (weak, nonatomic) IBOutlet UITextField *tfIntroBg;

@property(nonatomic) BOOL isIdLoaded;

@property(nonatomic, strong) MBProgressHUD *hud;

@property(nonatomic, strong) id facebookId;

- (IBAction)onBtnPress:(UIButton *)sender;
@end
