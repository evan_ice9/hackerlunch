//
//  UIImage+CustomMethods.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/7/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CustomMethods)

- (UIImage *)crop:(CGRect)rect;

@end
