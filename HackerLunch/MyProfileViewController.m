//
//  MyProfileViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 9/15/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "MyProfileViewController.h"
#import "UIImage+CustomMethods.h"
#import "ECSlidingViewController.h"

@interface MyProfileViewController ()

@end

@implementation MyProfileViewController {
    BOOL isDone;
    BOOL isViaFb;
    NSString *fbEmail;
    NSData *imageData;
    MBProgressHUD *hud;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    hud = [[MBProgressHUD alloc] initWithView:self.descView];

    self.descView.layer.borderWidth = 1.0f;
    self.descView.layer.borderColor = [UIColor lightGrayColor].CGColor;

    [self checkFbStat];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSettingCellTap:)];
    tap.numberOfTapsRequired = 1;
    tap.cancelsTouchesInView = YES;

    [self.btnDesc addGestureRecognizer:tap];
}

-(void)viewWillAppear:(BOOL) animated {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setProfilePicture];
}

- (void)checkFbStat{
    isViaFb = [[[UserProfile sharedInstance] userObjectForKey:kIsViaFacebook] boolValue];

    if(isViaFb) {
        fbEmail = [[UserProfile sharedInstance] userObjectForKey:kUserEmailForFb];

        self.sublabel.text = fbEmail;
        self.sublabel.hidden = NO;

        self.btnConnect.hidden = YES;
        self.btnClose.hidden = NO;
    }
}

- (IBAction)onButtPress:(UIButton *) sender {
    int tag = sender.tag;

    switch (tag) {
        case 19:
            //back btn
            self.descView.type = CSAnimationTypeFadeOut;
            [self.descView startCanvasAnimation];
            break;
        case 20:
            //save settings
            [self uploadData];
            break;
        case 21:
            //upload photo
            [self pickImageViaCamera:NO];
            break;
        case 22:
            //take photo
            [self pickImageViaCamera:YES];
            break;
        case 100:
            //take photo
            [self.slidingViewController anchorTopViewTo:ECRight];
            break;
        default:
            break;
    }
}

- (void)uploadData {
    if(self.input_description.text.length > 0 || imageData != nil){

        // MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
        hud.labelText = @"Preparing profile...";
        [self.view addSubview:hud];
        hud.delegate = self;
        [hud show:YES];

        PFUser *user = [PFUser currentUser];
        NSString *fullname = [[UserProfile sharedInstance] userObjectForKey:kUserFullName]; //[NSUserDefaults valueForKey:kUserFullName];

        if(user[@"fullName"] == nil)
            [user setObject:fullname forKey:@"fullName"];

        if(fbEmail)
            [user setObject:fbEmail forKey:@"fbEmail"];

        if(self.input_description.text.length > 0){
            NSLog(@"about me added");
            [user setObject:self.input_description.text forKey:@"about"];
        }

        if(imageData != nil){
            hud.mode = MBProgressHUDModeAnnularDeterminate;

            PFFile *imageFile = [PFFile fileWithName:@"Image.jpg" data:imageData];

            [imageFile saveInBackgroundWithBlock:^(BOOL success, NSError *err) {
                if(success) {
                    [user setObject:imageFile forKey:kUserProfilePicture];

                    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {

                        if(error || !succeeded) {
                            NSString *errorString = [error userInfo][@"error"];
                            [self showAlert:(error) ? errorString : @"Network Error."];
                        } else {
                            NSLog(@"done uploading image");

                            hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
                            hud.mode = MBProgressHUDModeCustomView;
                            hud.labelText = @"Done";
                            [hud hide:YES afterDelay:0.5];

                            isDone = YES;

                            //[NSUserDefaults setUserValue:imageData forKey:kLoadProPic];
                            [[UserProfile sharedInstance] setUserObject:imageData forKey:kLoadProPic];
                        }
                    }];

                } else {
                    [hud hide:YES];
                    NSString *errorString = [err userInfo][@"error"];
                    [self showAlert:(err) ? errorString : @"Network Error."];
                }

            } progressBlock:^(int percentDone) {
                hud.progress = (float) percentDone / 100;
            }];
        }else{
            [NSUserDefaults removeUserValueForKey:kLoadProPic];

            hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = @"Done";
            [hud hide:YES afterDelay:0.5];

            isDone = YES;
        }
    } else{
        [self toHome];
    }
}

- (void)linkToFb {
    NSLog(@"linking fb");

    hud.labelText = @"Linking profile...";
    [self.view addSubview:hud];
    hud.delegate = self;
    [hud show:YES];

    PFUser *user = [PFUser currentUser];
    if(![PFFacebookUtils isLinkedWithUser:user]) {
        [PFFacebookUtils linkUser:user permissions:nil block:^(BOOL succeeded, NSError *error) {
            [hud hide:YES];

            if(error || !succeeded){
                //[hud hide:YES];
                NSString *errorString = [error userInfo][@"error"];
                [self showAlert:(error) ? errorString : @"Network Error."];
            }else {
                if (!user) {
                    //[hud hide:YES];

                    NSLog(@"Uh oh. The user cancelled the Facebook login.");
                    [self showAlert:@"User cancelled the Facebook login"];

                } else {
                    NSLog(@"facebook linked!");
                    [self fetchFbInformation];
                    [self fetchFbPic];
                    [self showAlert:@"Successfully linked." title:@"Congratulation"];
                }
            }
        }];
    } else {
        [hud hide:YES];
        [self fetchFbInformation];
        [self fetchFbPic];
        [self showAlert:@"Account already linked." title:@"Info"];
    }
}

- (void)fetchFbInformation{
    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *err) {
        [hud show:NO];
        [hud removeFromSuperview];

        if (!err) {
            [[UserProfile sharedInstance] setUserObject:result[@"email"] forKey:kUserEmailForFb];
            [[UserProfile sharedInstance] setUserObject:result[@"id"] forKey:kFacebookId];
            [[UserProfile sharedInstance] setUserObject:@(YES) forKey:kIsViaFacebook];

            [self checkFbStat];
        }else{
            [self showAlert:err.userInfo[@"error"]];
        }
    }];
}

- (void)fetchFbPic{
    FBSession *fbSession = [PFFacebookUtils session];
    NSString *accessToken = [fbSession accessTokenData].accessToken;
    NSString *url = [NSString stringWithFormat:@"https://graph.facebook.com/me/picture?type=large&return_ssl_resources=1&access_token=%@", accessToken];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(picLoadDone:) name:AsyncImageLoadDidFinish object:nil];
    self.profilePicView.showActivityIndicator = YES;
    self.profilePicView.activityIndicatorStyle = UIActivityIndicatorViewStyleGray;
    self.profilePicView.imageURL = [NSURL URLWithString:url];

    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        [hud hide:YES];

        if(!error) {
            fbEmail = [result objectForKey:@"id"];
        }else{
            [self showAlert:@"Connection Error"];
        }
    }];
}

- (IBAction)onFbConnect:(id) sender {
    int tag = ((UIButton *) sender).tag;

    if(tag == 11) {
        //connect btn
        [self linkToFb];
    } else if(tag == 12) {
        //close btn
        [self unlinkFb];
    }
}

- (void)unlinkFb {
    PFUser *user = [PFUser currentUser];
    [PFFacebookUtils unlinkUserInBackground:user block:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            NSLog(@"The user is no longer associated with their Facebook account.");
            isViaFb = NO;
            fbEmail = @"";

            self.sublabel.text = fbEmail;
            self.sublabel.hidden = YES;

            self.btnConnect.hidden = NO;
            self.btnClose.hidden = YES;

            
            [[UserProfile sharedInstance] removeUserObjectForKey:kUserEmailForFb];
            [[UserProfile sharedInstance] removeUserObjectForKey:kFacebookId];
            [[UserProfile sharedInstance] removeUserObjectForKey:kIsViaFacebook];
        }
    }];
}

- (void)pickImageViaCamera:(BOOL) boool {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] && boool){
        NSLog(@"no cam, dude!");
        [self showAlert:@"No Camera!"];
        boool = NO;
    }

    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = boool ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *) picker didFinishPickingMediaWithInfo:(NSDictionary *) info {
    // Access the uncropped image from info dictionary
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];

    // Dismiss controller
    [picker dismissViewControllerAnimated:YES completion:nil];

    float iw = image.size.width;
    float ih = image.size.height;
    float ratio = iw/ih;
    float unitSize = 300;

    // Resize image
    UIGraphicsBeginImageContext(CGSizeMake(unitSize*ratio, unitSize));
    [image drawInRect: CGRectMake(0, 0, unitSize*ratio, unitSize)];
    UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    smallImage = [smallImage crop:CGRectMake((CGFloat) (unitSize * ratio * 0.5 - unitSize * 0.5), 0, unitSize, unitSize)];

    // Upload image
    imageData = UIImageJPEGRepresentation(smallImage, 0.95f);

    [[UserProfile sharedInstance] setUserObject:imageData forKey:kLoadProPic];
    self.profilePicView.image = smallImage;
}

- (void)onSettingCellTap:(UITapGestureRecognizer *) tap {
    self.input_description.text = [PFUser currentUser][@"about"];
    [self.input_description_bg setPlaceholder:@""];

    self.descView.type = CSAnimationTypeFadeIn;
    [self.descView startCanvasAnimation];
}

- (void)hudWasHidden:(MBProgressHUD *) _hud {
    NSLog(@"all set");
    [_hud removeFromSuperview];

    if(isDone) {
        [self toHome];
    }
}

- (void)toHome {
    self.descView.type = CSAnimationTypeFadeOut;
    [self.descView startCanvasAnimation];
}

- (void)setProfilePicture {
    imageData = [[UserProfile sharedInstance] userObjectForKey:kLoadProPic];

    if(imageData){
        UIImage *image = [UIImage imageWithData:imageData];
        [self.profilePicView setImage:image];
    }else{
        PFUser *user = [PFUser currentUser];
        PFFile *file = user[kUserProfilePicture];
        
        NSString *url = file.url;
        
        if(url){
            self.profilePicView.imageURL = [[NSURL alloc] initWithString:url];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(picLoadDone:) name:AsyncImageLoadDidFinish object:nil];            
        }
    }
}

- (void)picLoadDone:(NSNotification *) not {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AsyncImageLoadDidFinish object:nil];

    imageData = [NSData dataWithData:UIImagePNGRepresentation(self.profilePicView.image)];

    [[UserProfile sharedInstance] setUserObject:imageData forKey:kLoadProPic];
}

- (void)showAlert:(NSString *) message {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Error" andMessage:message];
    [alertView addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeDestructive handler:nil];
    [alertView show];
}

- (void)showAlert:(NSString *) message title:(NSString *) title {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:title andMessage:message];
    [alertView addButtonWithTitle:@"OK" type:SIAlertViewButtonTypeDestructive handler:nil];
    [alertView show];
}

- (void)textViewDidBeginEditing:(UITextView *) textView {
    [self.input_description_bg setPlaceholder:@""];
}

- (BOOL)textViewShouldEndEditing:(UITextView *) textView {
    [self.view endEditing:YES];
    return YES;
}

- (void)touchesEnded:(NSSet *) touches withEvent:(UIEvent *) event {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
