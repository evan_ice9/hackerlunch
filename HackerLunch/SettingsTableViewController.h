//
//  SettingsTableViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/17/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+APUtils.h"
#import "ECSlidingViewController.h"
#import "MyProfileViewController.h"
#import "HistoryViewController.h"
#import "PrefsViewController.h"

@interface SettingsTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UIImageView *profilePicView;
@property (strong, nonatomic) IBOutlet UILabel *fullNameView;

-(void) setHomeView:(UIViewController *) vc;

@end
