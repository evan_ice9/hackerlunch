//
//  FindingViewController.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/20/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Canvas.h"
#import "MotherViewController.h"
#import "ECSlidingViewController.h"

@interface FindingViewController : MotherViewController

@property (strong, nonatomic) IBOutlet CSAnimationView *findingView;
@property (strong, nonatomic) IBOutlet CSAnimationView *noFindView;

- (IBAction)onSideBtn:(UIButton *)sender;
- (IBAction)onFbInvite:(id)sender;

@end
