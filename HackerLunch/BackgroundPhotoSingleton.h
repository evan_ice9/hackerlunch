//
//  BackgroundPhotoSingleton.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/5/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBG.h"
#import "GPUImage.h"

@interface BackgroundPhotoSingleton : UIViewController

@property(nonatomic, weak) NSTimer *timer;
@property(nonatomic, strong) NSURL *userPhotoWebPageURL;
@property(nonatomic, strong) UIImage *lastImage;

+ (BackgroundPhotoSingleton *) sharedInstance;
-(void) startScheduleWithIntervalInSeconds:(uint) kTime;

- (void)retrieveLocationAndUpdateBackgroundPhoto;
@end
