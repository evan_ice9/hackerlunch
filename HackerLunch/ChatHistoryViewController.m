//
//  ChatHistoryViewController.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 9/8/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "ChatHistoryViewController.h"

@interface ChatHistoryViewController ()

@end

@implementation ChatHistoryViewController {
    MBProgressHUD *hud;

//    NSMutableArray *channelList;
//    NSMutableArray *alerts;
//    NSMutableArray *lastPrios;
//    NSMutableArray *texts;
//    NSMutableArray *chatStarted;

    NSMutableArray *tempInfo;
    NSMutableArray *mainInfo;

    NSInteger alertCounter;

    FirebaseHandle handle;

   // BOOL inited;
    BOOL isRunning;
    BOOL showNotification;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    tempInfo = [[NSMutableArray alloc] init];
    mainInfo = [[NSMutableArray alloc] init];

    hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.labelText = @"Fetching data...";
    [self.view addSubview:hud];
    [hud show:YES];

    isRunning = NO;

    [self getChannelsList];

    [NSTimer scheduledTimerWithTimeInterval:30.0f
                                     target:self
                                   selector:@selector(getChannelsList)
                                   userInfo:nil
                                    repeats:YES];

}

- (void)viewWillAppear:(BOOL) animated {
    [self getChannelsList];
}

- (void)getChannelsList {
    if(isRunning)
        return;

    isRunning = YES;

    PFUser *user = [PFUser currentUser];
    PFQuery *query1 = [PFQuery queryWithClassName:class_chatlog];

    [query1 whereKey:@"channelName" containsString:user.objectId];

    [query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(error){
            [hud hide:YES];
            isRunning = NO;

            [self showOnErrorButtons];
        }

        if(objects.count > 0){
            [tempInfo removeAllObjects];
            alertCounter = 0;

            NSMutableArray *tmpArr = [[NSMutableArray alloc] init];

            for(PFObject *pob in objects){
                PFUser *pUser = [self getPairedUser:pob currentUser:user];
                NSString *channelName = pob[@"channelName"];
                BOOL inited = [pob[@"inited"] boolValue];
                NSInteger lastId = pob[@"lastMessageId"] == nil ? 0 : [pob[@"lastMessageId"] integerValue];

                if(inited) alertCounter++;

                NSMutableDictionary *info = [[NSMutableDictionary alloc] init];

                info[@"channelName"] = channelName;
                info[@"inited"] = @(inited);
                info[@"lastMessageId"] = @(lastId);
                info[@"user"] = pUser;

                PFQuery *q = [PFUser query];
                [q whereKey:@"objectId" equalTo:pUser.objectId];
                [tmpArr addObject:q];

                [tempInfo addObject:info];
            }

            //fetch user infos
            PFQuery *query2  = [PFQuery orQueryWithSubqueries:tmpArr];
            [query2 findObjectsInBackgroundWithBlock:^(NSArray *objs, NSError *err) {
                //info[@"user"] = objs;
                if(err){
                    [hud hide:YES];
                    isRunning = NO;

                    [self showOnErrorButtons];
                }

                if(objects.count > 0){

                    //Add userinfo to array
                    for (int i = 0; i < objs.count; ++i) {
                        PFUser *u = objs[i];
                        [self findAndReplace:u];
                      //  tempInfo[i] = info;
                    }

                    [self fetchChannelData];

                }else{
                    self.pairInfoLabel.hidden = NO;
                    isRunning = NO;
                    [hud hide:YES];
                }
            }];
        } else {
            [hud hide:YES];
            isRunning = NO;

            self.pairInfoLabel.hidden = NO;
        }
    }];
}

- (void)findAndReplace:(PFUser *) user {
    for(int i = 0; i < tempInfo.count; i++){
        NSMutableDictionary *d = tempInfo[i];
        PFUser *tUser = d[@"user"];

        NSString *tid = tUser.objectId;
        NSString *oid = user.objectId;

        if([oid isEqualToString:tid]) {
            PFFile *file = user[@"profilePicture"];
            NSString *url = (file.url) ? file.url : @"";

            d[@"user"] = user;
            d[@"imageUrl"] = url;

            tempInfo[i] = d;

            return;
        }
    }
}

- (void)fetchChannelData {
//  Remainings
//    [alerts removeAllObjects];
//    [lastPrios removeAllObjects];
//    [texts removeAllObjects];

    PFUser *user = [PFUser currentUser];
    __block int z = tempInfo.count;

    for(int i = 0; i < tempInfo.count; i++){
        NSMutableDictionary *info = tempInfo[i];
        NSString *channelName = info[@"channelName"];
        BOOL chatStarted = [info[@"inited"] boolValue];
      //  NSInteger sid = [info[@"lastMessageId"] integerValue];

        //check for empty channels
        if(chatStarted){
            Firebase *firebase = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@/%@", FIREBASE, channelName]];
            FQuery *postsQuery = [firebase queryLimitedToNumberOfChildren:1];


            //add listerners
            handle = [postsQuery observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
                //[firebase removeObserverWithHandle:handle];

                FDataSnapshot *snap = snapshot;//.children.allObjects.firstObject;

                NSString *uid = snap.value[@"uid"];
                NSString *text = snap.value[@"text"];
                NSInteger lastPrio = [[snap.value objectForKey:@"priority"] integerValue];
                NSInteger savedMsgPrio = [[UserProfile sharedInstance] getLastMsgId:channelName];

                //if(savedMsgPrio == (NSInteger) nil) savedMsgPrio = sid;

                info[@"text"] = text;
                info[@"lastMessageId"] = @(lastPrio);
                info[@"alert"] = @(NO);

                //check if self msg
                if(![uid isEqualToString:user.username] && lastPrio > savedMsgPrio) {
                    info[@"alert"] = @(YES);

                    //send notification to SHOW outer alert
                    [[NSNotificationCenter defaultCenter] postNotificationName:CHAT_NOTIFICATION object:@(YES) userInfo:nil];
                }

                //tempInfo[i] = info;
                [self findAndReplaceInfo:info];

                if(z == alertCounter) {
                    [hud hide:YES];
                    isRunning = NO;

                    [mainInfo removeAllObjects];
                    [mainInfo addObjectsFromArray:tempInfo];

                    //[firebase removeAllObservers];

//                    if(lastSize != tempInfo.count || lastSize == -1){
//                        lastSize = tempInfo.count;
//                        [self updateListeners];
//                    }

                    [self.tableView reloadData];
                    [self checkAlerts];
                }
            }];
        } else{
            info[@"alert"] = @(NO);
            info[@"lastMessageId"] = @(-1);
            info[@"text"] = @"";

            z--;
        }

        tempInfo[i] = info;
    }

    if(z == 0){
        [hud hide:YES];
        isRunning = NO;

        [mainInfo removeAllObjects];
        [mainInfo addObjectsFromArray:tempInfo];
        [self.tableView reloadData];

        //send notification to HIDE outer alert
        [[NSNotificationCenter defaultCenter] postNotificationName:CHAT_NOTIFICATION object:@(NO) userInfo:nil];
    }
}

//- (void)updateListeners {
//    PFUser *user = [PFUser currentUser];
//
//    for(int i = 0; i < mainInfo.count; i++) {
//        NSMutableDictionary *info = mainInfo[i];
//        NSString *channelName = info[@"channelName"];
//
//        Firebase *firebase = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@/%@", FIREBASE, channelName]];
//        FQuery *postsQuery = [firebase queryLimitedToNumberOfChildren:1];
//
//        [postsQuery observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
//            NSString *sender = snapshot.value[@"uid"];
//
//            if(![user.username isEqualToString:sender]) {
//                NSMutableDictionary *inf = mainInfo[i];
//                inf[@"alert"] = @(YES);
//                mainInfo[i] = inf;
//
//                [self.tableView reloadData];
//
//                //send notification to SHOW outer alert
//            }
//        }];
//    }
//}

- (void)findAndReplaceInfo:(NSMutableDictionary *) info{
    for(int i = 0; i<tempInfo.count; i++){
        NSDictionary *d = tempInfo[i];
        NSString *ch = d[@"channedName"];

        NSString *target_ch = info[@"channedName"];

        if([ch isEqualToString:target_ch]){
            tempInfo[i] = info;
            return;
        }
    }
}

/*- (void)addChannelListeners {
    alerts = [[NSMutableArray alloc] initWithCapacity:channelList.count];
    texts = [[NSMutableArray alloc] initWithCapacity:channelList.count];
    lastPrios = [[NSMutableArray alloc] initWithCapacity:channelList.count];
    //NSMutableArray *refs = [[NSMutableArray alloc] initWithCapacity:channelList.count];

    __block int z = channelList.count;
    PFUser *user = [PFUser currentUser];

    for(int i = 0; i<channelList.count; i++){
        PFObject *po = channelList[i];
        NSString *channelName = po[@"channelName"];

        Firebase *firebase = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@/%@", FIREBASE, channelName]];
        FQuery *postsQuery = [firebase queryLimitedToNumberOfChildren:1];

        if([po[@"inited"] integerValue] == 0)
            z--;

        handle = [postsQuery observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
            [firebase removeObserverWithHandle:handle];

            NSInteger lastPrio = [[snapshot.value objectForKey:@"priority"] integerValue];
            NSInteger msgId = [[UserProfile sharedInstance] getLastMsgId:channelName];
            NSString *uid = snapshot.value[@"uid"];

            [lastPrios addObject:@(lastPrio)];

            BOOL b = YES;
            if(msgId < lastPrio) {
                b = [uid caseInsensitiveCompare:user.username] == NSOrderedSame;
            }
            else if(msgId >= lastPrio)
                b = YES;

            [alerts addObject:@(b)];
            [texts addObject:[snapshot.value objectForKey:@"text"]];

//                alerts[i] = @(msgId >= lastPrio);
            //              texts[i] = [snapshot.value objectForKey:@"text"];

            if(z == alerts.count) {
                inited = YES;
                [self.tableView reloadData];

                [hud hide:YES];
                isRunning = false;

                for(int k = 0; k < channelList.count; k++){
                    PFObject *p = channelList[k];

                    if([p[@"inited"] integerValue] == 0){
                        [alerts insertObject:[NSNull null] atIndex:k];
                        [texts insertObject:[NSNull null] atIndex:k];
                        [lastPrios insertObject:[NSNull null] atIndex:k];
                    }
                }

                [self checkAlerts];
            }

          //  NSLog(@"%i %i %i", alerts.count, channelList.count, z);
        }];
        
       // NSLog(@"---------> %i", z);
    }
    
    if(z == 0){
        [hud hide:YES];
        isRunning = false;
        
        [self.tableView reloadData];
    }
}*/

- (PFUser *)getPairedUser:(PFObject *) object currentUser:(PFUser *) user {
    PFUser *userA = object[@"userA"];
    PFUser *userB = object[@"userB"];
    NSString *oid = userA.objectId;

    if([user.objectId isEqualToString:oid]) return userB;
    else return userA;
}

- (void)checkAlerts {
    for(int i = 0; i < mainInfo.count; i++){
        NSDictionary *d = mainInfo[i];
        BOOL b = [d[@"alert"] boolValue];

        if(b){
            [[NSNotificationCenter defaultCenter] postNotificationName:CHAT_NOTIFICATION object:@(YES) userInfo:nil];
            return;
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:CHAT_NOTIFICATION object:@(NO) userInfo:nil];
}


- (NSInteger)tableView:(UITableView *) tableView numberOfRowsInSection:(NSInteger) section {
    return mainInfo.count;
}

- (UITableViewCell *)tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *) indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];

    UILabel *name = (UILabel *) [cell viewWithTag:11];
    UILabel *text = (UILabel *) [cell viewWithTag:13];
    AsyncImageView *img = (AsyncImageView *) [cell viewWithTag:10];
    UIView *alert = [cell viewWithTag:12];

    @try {
        NSMutableDictionary *info = mainInfo[indexPath.row];
        PFUser *user = info[@"user"];
        NSString *url = info[@"imageUrl"];
        BOOL bAlert = [info[@"alert"] boolValue];

        if(user[kUserFullName] != nil)
            name.text = user[kUserFullName];

        PFFile *userImageFile = user[kUserProfilePicture];

        if(![url isEqualToString:@""])
            img.imageURL = [[NSURL alloc] initWithString:userImageFile.url];

        text.text = info[@"text"];
        alert.hidden = !bAlert;

    }@catch (NSException *ex){
        return cell;
    }

    return cell;
}


- (void)tableView:(UITableView *) tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *) indexPath {
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithHexString:@"222222"];
    [cell setSelectedBackgroundView:bgColorView];
}


- (void)tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *) indexPath {
    NSMutableDictionary *info = mainInfo[indexPath.row];
    //info[@"pfUser"] = userList[indexPath.row];

    @try {
        info[@"alert"] = @(NO);

        int cid = [info[@"lastMessageId"] integerValue];
        [[UserProfile sharedInstance] setLastMsgId:cid Key:info[@"channelName"]];
    }@catch (NSException *ex){
        NSLog(@"ERROR: alerts out of bound.");
    }

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    dictionary[@"chatroom"] = info[@"channelName"];

    [[NSNotificationCenter defaultCenter] postNotificationName:LAUNCH_CHAT_VIEW object:info[@"user"] userInfo:dictionary];

    mainInfo[indexPath.row] = info;
    [self.tableView reloadData];

    [self checkAlerts];
}


- (IBAction)onErrButt:(id)sender {
    [self hideOnErrorButtons];
    [self getChannelsList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)hudWasHidden:(MBProgressHUD *) mHud {
    [mHud removeFromSuperview];
}

- (void) showOnErrorButtons {
    self.errButt.hidden = NO;
    self.errButt.userInteractionEnabled = YES;
}

- (void) hideOnErrorButtons {
    self.errButt.hidden = YES;
    self.errButt.userInteractionEnabled = NO;
}

@end
