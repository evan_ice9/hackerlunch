//
//  UIView+CustomFormat.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/6/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "UIView+CustomFormat.h"

@implementation UIView (CustomFormat)

- (NSString *) cornerRadiusSize {
    return self.cornerRadiusSize;
}

- (void) setCornerRadiusSize:(NSString *)cornerRadiusSize {
    self.layer.cornerRadius = [cornerRadiusSize floatValue];
}


@end
