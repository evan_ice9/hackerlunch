//
//  UIImageView+CustomFormat.h
//  HackerLunch
//
//  Created by Fahim Ahmed on 7/6/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (CustomFormat)

@property (nonatomic, copy) NSString* cornerRadiusSize;

@end
