//
//  Network.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 1/17/15.
//  Copyright (c) 2015 Fahim Ahmed. All rights reserved.
//

#import "Network.h"

@implementation Network



+(Network *)Shared_Client
{

    static Network *shared_network=nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        
        shared_network =[[self alloc]init];
        
    });

    return shared_network;
}


-(instancetype)initWithBaseURL:(NSURL *)url
{

    self=[super initWithBaseURL:url];
    
    if (self)
    {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }

    return self;
}




-(void)loginwithParameters:(NSDictionary *)data completionBlock:(void (^)(int status,id response))compblock
{

    
    
    [self POST:@"login" parameters:data success:^(NSURLSessionDataTask *task, id responseObject)
    {
        NSLog(@"%@",responseObject);
        
        if ([responseObject objectForKey:@"msg"])
        {
            compblock(1,nil);
        }
        else
        {
          compblock(0,responseObject);
            
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        NSLog(@"%@",error.description);
            compblock(2,error.description);
        
    }];




}


-(void)SignupwithParameters:(NSDictionary *)data
{


    [self POST:@"Register" parameters:data success:^(NSURLSessionDataTask *task, id responseObject)
     {
         NSLog(@"%@",responseObject);
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         NSLog(@"%@",error.description);
         
     }];



}
@end
