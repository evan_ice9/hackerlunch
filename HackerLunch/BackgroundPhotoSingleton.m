//
//  BackgroundPhotoSingleton.m
//  HackerLunch
//
//  Created by Fahim Ahmed on 8/5/14.
//  Copyright (c) 2014 Fahim Ahmed. All rights reserved.
//

#import "BackgroundPhotoSingleton.h"

//#define kTimerIntervalInSeconds 300 //10min

@implementation BackgroundPhotoSingleton {
    uint t;
}

+ (BackgroundPhotoSingleton *) sharedInstance {
    static BackgroundPhotoSingleton *_instance = nil;

    @synchronized (self) {
        if(_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

-(void) startScheduleWithIntervalInSeconds:(uint) kTime {
    t = kTime;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:kTime target:self selector:@selector(retrieveLocationAndUpdateBackgroundPhoto) userInfo:nil repeats:YES];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];

}

- (void) retrieveLocationAndUpdateBackgroundPhoto {
    NSLog(@"bg image fetch initiated");
    
    //Location
    [[CBGLocationManager sharedManager] locationRequest:^(CLLocation * location, NSError * error) {
        if(!error) {
            //Flickr
            [[CBGFlickrManager sharedManager] randomPhotoRequest:^(FlickrRequestInfo *flickrRequestInfo, NSError *err) {
                if(!err) {
                    self.userPhotoWebPageURL = flickrRequestInfo.userPhotoWebPageURL;
                    //[self crossDissolvePhotos:flickrRequestInfo.photos withTitle:flickrRequestInfo.userInfo];

                    self.lastImage = flickrRequestInfo.photos.photo;
                    GPUImageGrayscaleFilter *gFilter = [[GPUImageGrayscaleFilter alloc] init];
                    self.lastImage = [gFilter imageByFilteringImage:self.lastImage];

                    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                    dict[kPhotos] = self.lastImage;//flickrRequestInfo.photos.photo;
                    [[NSNotificationCenter defaultCenter] postNotificationName:kBackgroundImageLoadedNotification object:nil userInfo:dict];

                }else{
                    NSLog(@"error %i: %@", error.code, error.userInfo);
                }
            }];
        }else{
            NSLog(@"error %i: %@", error.code, error.description);
        }
    }];
}

//- (void) crossDissolvePhotos:(CBGPhotos *) photos withTitle:(NSString *) title {
//    [UIView transitionWithView:self.backgroundPhoto duration:1.0f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
//
//        self.backgroundPhoto.image = photos.photo;
//        //  self.backgroundPhotoWithImageEffects.image = photos.photoWithEffects;
//        //  self.photoUserInfoBarButton.title = title;
//    } completion:NULL];
//}


- (void)appWillEnterForeground:(NSNotification *) note {
    [self startScheduleWithIntervalInSeconds:t];

    [[UserProfile sharedInstance] goOnline];
}

-(void) appWillResignActive:(NSNotification*) note {
    NSLog(@"going to lost focus :(");
    [self.timer invalidate];

    [[UserProfile sharedInstance] goOffline];
}

-(void)appWillTerminate:(NSNotification*) note {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
}

@end
